---
production:
  analytics:
    api_key: "your-analytics-apikey"
    api_base: "/almaws/v1"
    format: "xml"
    host: "your-alma-api-proxy-server"
    timeout: 10
    tries: 3
    root: "/analytics/reports"
