#!/bin/sh

install_base=$1
if [ "$install_base" == "" ]; then
	echo "You did not specify the install base directory."
	echo "Usage: copycon.sh <install-base-directory>"
	exit
fi

for i in archives conf data logs share; do
	cp -pr $i $install_base
done
