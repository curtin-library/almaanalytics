
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Globals::Constants;

use strict;
use warnings;

use Alma2::Defaults;

# ========================================================================================================================

# pin the root down
use constant ALMA_HOME => Alma2::Defaults::ALMA_HOME;					# Alma2 home / root directory
use constant ALMA_ARCHIVES => Alma2::Defaults::ARCHIVES;				# Alma2 archive directory
use constant LOG_ROOT => Alma2::Defaults::LOG_ROOT . "/logs";				# Alma2 logging directory
use constant CONFIG_FILE => Alma2::Defaults::CONFIG_DIR . "/config.yml";	# Alma2 common configuration file
use constant EMAIL_SYSTEM => Alma2::Defaults::EMAIL_SYSTEM;			# System email standard From: address

# ========================================================================================================================

1;
