
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::REST::Error;

use strict;
use warnings;
use Data::Dumper;

use Switch;
use LWP::UserAgent;
use REST::Client;
use POSIX;

use XML::LibXML;
use Encode;
use JSON;

# ========================================================================================================================

sub new {
	my ($class, $ctype, $content) = @_;

	my $self = {
		errors => undef,
	};
	bless $self, $class;

	$self->parse_content($ctype, $content);
}

# ========================================================================================================================

sub get {
	my ($self) = @_;
	return $self->{errors};
}

# ========================================================================================================================

sub parse_error_xml {
	my ($self, $xml) = @_;

	# ==========
	# Sample XML error responses:
	# --------------------------
	# <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
	# <web_service_result xmlns="http://com/exlibris/urm/general/xmlbeans">
	#   <errorsExist>true</errorsExist>
	#   <errorList>
	#     <error>
	#       <errorCode>401861</errorCode>
	#       <errorMessage>User with identifier abc123 was not found. (Tracking ID: E01-0903073303-3ABIZ-AWAE1730827399)      </errorMessage>
	#     </error>
	#     <error>
	#       <errorCode>INTERNAL_SERVER_ERROR</errorCode>
	#       <errorMessage>The web server encountered an unexpected condition that prevented it from fulfilling the request. If the error persists please use this unique tracking ID when reporting it: E01-2303131047-FGBKX-TSE188753689</errorMessage>
	#     </error>
	#   </errorList>
	# </web_service_result>
	#
	# XPaths to look for:
	# ------------------
	# /web_service_result/errorsExist == true
	# /web_service_result/errorList/error/errorCode -> code
	# /web_service_result/errorList/error/errorMessage -> message
	# ==========

	my $dom = XML::LibXML->load_xml(string => $xml);
	my $xpath = XML::LibXML::XPathContext->new();
	$xpath->registerNs('x', "http://com/exlibris/urm/general/xmlbeans");

	my ($node) = $xpath->findnodes('/x:web_service_result/x:errorsExist', $dom);
	my $isError = ($node->textContent =~ /true/ ? 1 : 0);
	return undef if !$isError;

	my @errors = ();
	my @array = $xpath->findnodes('/x:web_service_result/x:errorList/x:error', $dom);
	foreach my $enode (@array) {
		($node) = $xpath->findnodes('x:errorCode', $enode);
			my $code = $node->textContent;
		($node) = $xpath->findnodes('x:errorMessage', $enode);
			my $message = $node->textContent;

		push @errors, { code => $code, message => $message, };
	}

	return \@errors;
}

sub parse_error_json {
	my ($self, $json) = @_;

	#	$result => {
	#		errorsExist => 1,
	#		errorList => {
	#			error => [
	#				{ errorCode => 401861, errorMessage => "User with identifier abc123 was not found. (Tracking ID: E01-0903073303-3ABIZ-AWAE1730827399)      ", },
	#				{ errorCode => 'INTERNAL_SERVER_ERROR', errorMessage => "The web server encountered an unexpected condition that prevented it from fulfilling the request. If the error persists please use this unique tracking ID when reporting it: E01-2303131047-FGBKX-TSE188753689", },
	#			],
	#		},
	#	}

	my $joe = JSON->new;
	my $result = $joe->decode($json);
	return undef if ( $result->{'errorsExist'} ne '1' );

	my @errors = ();
	my $errors = $result->{'errorList'}->{'error'};
	foreach my $error (@$errors) {
		my $code = $error->{'errorCode'};
		my $message = $error->{'errorMessage'};
		push @errors, { code => $code, message => $message, };
	}

	return \@errors;
}

sub parse_content {
	my ($self, $ctype, $content) = @_;

	my $parsed = [];

	if ( $ctype =~ /xml/i ) {
		$parsed = $self->parse_error_xml($content);
	} elsif ( $ctype =~ /json/i ) {
		$parsed = $self->parse_error_json($content);
	}

	# save error
	$self->{errors} = $parsed;
}

# ========================================================================================================================

1;
