
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

# ========================================================================================================================
# Alma2::REST::Client->new($api_conf); new - apikey
#
# # C = Create / POST   : Alma2::REST::Client->POST   <=> PUT    (path)
# # R = Read / GET      : Alma2::REST::Client->GET    <=> GET    (path)
# # U = Update / PUT    : Alma2::REST::Client->PUT    <=> POST   (path)
# # D = Delete / DELETE : Alma2::REST::Client->DELETE <=> DELETE (path)
#
# NOTES:
# 1. each specific service (Analytics, Courses, Users) extends Alma2::REST::Client and implements its own logic
#    - handles behaviour specific to the service
#      - eg Analytics handles tokens and continuation
#      - eg Courses reads all of a course hierarchy in one go and reads recursively (get course -> get reading lists -> get reading list citations)
#    - handles errors specific to service
#
# 2. LWP::Protocol::https::Socket: SSL connect attempt failed with
#    "unknown error error:0D0890A1:asn1 encoding routines:ASN1_verify:unknown message digest algorithm
#     at /opt/local/lib/perl5/site_perl/5.8.9/LWP/Protocol/http.pm line 51."
#    - this was caused by setting (ssl_opts => { verify_hostname => 1 }) when no valid SSL_ca_file is specified
#    - therefore always set verify_hostname => 0
# ========================================================================================================================

package Alma2::REST::Client;

use strict;
use warnings;
use Data::Dumper;

#	use Exporter;
#	my @ERRORS = qw(
#		ALMAERR_INTERNAL_SERVER_ERROR
#		ALMERR_NO_ERROR
#		ALMAERR_AUTH_ERROR
#		ALMAERR_CLIENT_SIDE_ERROR
#		ALMAERR_INTERNAL_SERVER_ERROR
#		ALMAERR_HTTP_ERROR
#	);
#	my @CONSTANTS = qw(
#		ALMA_REST_DEFAULT_TIMEOUT
#		@ERRORS
#	);x
#	my @EXPORT_OK = qw( @CONSTANTS );
#	my %EXPORT_TAGS = (
#		constants => [ @CONSTANTS ],
#		errors => [ @ERRORS ],
#	);

use Switch;
use LWP::UserAgent;
use REST::Client;
use POSIX;
use URI;
use URI::Escape qw(uri_escape uri_escape_utf8);

use XML::LibXML;
use Encode;
use JSON;

use Alma2::Error;
use Alma2::REST::Error;


# ========================================================================================================================

use constant ALMA_REST_DEFAULT_TIMEOUT => 10;

# for 4XX (400,404, etc) error messages no content is returned so we manufacture it
use constant XML_4XX_ERR => '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' . "\n" .
	'<web_service_result xmlns="http://com/exlibris/urm/general/xmlbeans">' . "\n" .
	'<errorsExist>true</errorsExist>' . "\n" .
	'<errorList>' . "\n" .
	'<error>' . "\n" .
	'<errorCode>UNSPECIFIED ERROR</errorCode>' . "\n" .
	'<errorMessage></errorMessage>' . "\n" .
	'</error>' . "\n" .
	'</errorList>' . "\n" .
	'</web_service_result>';


# ========================================================================================================================

#	%options = (
#		host => 'https://api-ap.hosted.exlibrisgroup.com',
#		timeout => 10,
#		apikey => 'l7xxfe43bc15679d4edfa2e548dc8ea84df2',
#		format => 'json',
#		basepath => '/almaws/v1',
#	);

sub new {
	my ($class, $config) = @_;

	my $basepath = $config->{api_base};
	my $host = $config->{host};
	my $timeout = ( defined $config->{timeout} ? $config->{timeout} : ALMA_REST_DEFAULT_TIMEOUT );
	my $apikey = $config->{api_key};
	my $format = $config->{format};

	# create and configure the Alma2::REST::Client
	my $client = REST::Client->new();
 	my $self = bless {
		# REST::Client creation parameters
		host => $host,
		timeout => $timeout,
		tries => 3,
		ssl_opts => { verify_hostname => 0 },
		agent => 'LibraryApp/0.02',
		client => $client,
		basepath => $basepath,

		# common request parameters
		apikey => $apikey,
		format => $format,

		# members that change, request to request
		error => undef,
		api_errors => undef,
	}, $class;

	my $ua = $client->getUseragent();
	$ua->timeout( $self->{timeout} );
	$ua->ssl_opts( %{ $self->{ssl_opts} } );
	$ua->agent( $self->{agent} );

	return $self;
}

# ========================================================================================================================

sub host {
	$_[0]->{host};
}

sub timeout {
	$_[0]->{timeout};
}

sub ssl_opts {
	$_[0]->{ssl_opts};
}

sub agent {
	$_[0]->{agent};
}

sub client {
	$_[0]->{client};
}

sub basepath {
	$_[0]->{basepath};
}

sub apikey {
	$_[0]->{apikey};
}

sub format {
	$_[0]->{format};
}

sub response {
	$_[0]->{client}->responseContent();
}

sub error {
	$_[0]->{error};
}

sub api_errors {
	$_[0]->{api_errors};
}

# ========================================================================================================================

sub is_json {
	($_[0]->{format} =~ /json/i ? 1 : 0);
}

sub is_no_data {
	($_[0]->{error}->code() == 204 ? 1 : 0);
}

sub is_error {
	($_[0]->{error}->error() ? 1 : 0);
}

# ========================================================================================================================

sub encode_url {
	my ($self, $path, $params) = @_;

	# list-ify params hashref
	my %params = ();
	if (defined $params) {
		#%params = map { $_ => uri_escape($params->{$_}) } keys %$params;
		%params = map { $_ => $params->{$_} } keys %$params;
	}

	# format the URL + parameters
	my $uri = URI->new;
	$uri->path($path);
	$uri->query_form(\%params);
	my $url = $self->{host} . $self->{basepath} . $uri->path . (defined $uri->query ? '?' . $uri->query : '');

	return $url;
}

sub encode_content {
	my ($self, $content, $headers) = @_;

	# handle the content
	my $contents = Encode::encode('UTF-8', $content);
	#if ( $self->{format} eq 'json' ) {
	#	$contents = JSON->new()->encode($content);
	#} elsif ( $self->{format} eq 'xml' ) {
	#	$contents = Encode::encode('UTF-8', $content);
	#} else {
	#	$contents = $content;
	#}

	return $contents;
}

sub set_headers {
	my ($self, $headers) = @_;

	$headers = {} if !defined $headers;

	# API key
	$headers->{'Authorization'} = "apikey $self->{apikey}";

	# data format (JSON or XML)
	if ( $self->{format} eq 'json' ) {
		$headers->{'Content-Type'} = 'application/json';
		$headers->{'Accept'} = 'application/json';
	} elsif ( $self->{format} eq 'xml' ) {
		$headers->{'Content-Type'} = 'application/xml';
		$headers->{'Accept'} = 'application/xml';
	}

	return $headers;
}

# ========================================================================================================================

sub print_error {
	my ($self, $log, $message, $file, $func, $line) = @_;

	if ( $self->error()->error() ) {
		my $error = $self->error();
		my $rc = $error->error();
		my $http_code = $error->code();
		my $reason = $error->reason();
		my $msg = $error->message();
		$log->error("$message ($http_code:$rc:$reason:$msg)", "$file:$func", $line);

		my $api_errors = $self->api_errors();
		$log->debug(Dumper($api_errors), "$file:$func", $line);

		my $rsp = $self->response();
		$log->debug(Dumper($rsp), "$file:$func", $line);
		return 1;
	} else {
		return '';
	}
}


# ========================================================================================================================

sub parse_response {
	my ($self) = @_;

	my $client = $self->{client};

	# turn REST::Client response into an Alma2::Error
	my $error = Alma2::Error->new($client);
	$self->{error} = $error;
	return 0 if !$error->error();

	if ($error->code() !~ /^4/) {
		# analyse the API errors if there was errors
		my $ctype = $client->responseHeader('Content-Type');
		my $content = $client->responseContent();
		$self->{api_errors} = Alma2::REST::Error->new($ctype, $content);
	} else {
		# use the manufactured (unspecified) error for 4** HTTP response codes
		$self->{api_errors} = Alma2::REST::Error->new('xml', XML_4XX_ERR);
	}

	return ( $self->{error}->error() );
}

# ========================================================================================================================

sub create {
	my ($self, $path, $params, $body, $headers) = @_;

	# format parameters
	my $url = $self->encode_url($path, $params);

	# handle the content
	my $contents = $self->encode_content($body, $headers);

	# handle the headers
	my $hdrs = $self->set_headers($headers);

	# execute the request (POST for create)
	my $client = $self->{client};
	my $tries = $self->{tries};
	my $rc;
	for (my $i = 0; $i < $tries; $i++) {
		$client->POST($url, $contents, $hdrs);
		$rc = $self->parse_response();
		my $err = $self->{error};
		last if ($err->error() == Alma2::Error::ALMAERR_NO_ERROR);
	}

	# return OK = 0. !OK = 1.
	return $rc;
}

# ========================================================================================================================

sub read {
	my ($self, $path, $params, $headers) = @_;

	# format parameters
	my $url = $self->encode_url($path, $params);

	# handle the headers
	my $hdrs = $self->set_headers($headers);

	# execute the request (POST for create)
	my $client = $self->{client};
	my $tries = $self->{tries};
	my $rc;
	for (my $i = 0; $i < $tries; $i++) {
#print "Alma2::REST::Client::read(BEFORE):$url\n";
		$client->GET($url, $hdrs);
#print "Alma2::REST::Client::read(AFTER) :".$client->{_res}->{_request}->{_uri}."\n";
		$rc = $self->parse_response();
		my $err = $self->{error};
		last if ($rc < 0);
		last if ($err->error() == Alma2::Error::ALMAERR_NO_ERROR);
	}

	# return OK = 0. !OK = 1.
	return $rc;
}

# ========================================================================================================================

sub update {
	my ($self, $path, $params, $content, $headers) = @_;

	# format full path base
	my $url = $self->encode_url($path, $params);

	# encode contents
	my $contents = $self->encode_content($content, $headers);

	# handle the headers
	my $hdrs = $self->set_headers($headers);

	# execute the request (POST for create)
	my $client = $self->{client};
	my $tries = $self->{tries};
	my $rc;
	for (my $i = 0; $i < $tries; $i++) {
		$client->PUT($url, $contents, $hdrs);
		$rc = $self->parse_response();
		my $err = $self->{error};
		last if ($err->error() == Alma2::Error::ALMAERR_NO_ERROR);
	}

	# return OK = 0. !OK = 1.
	return $rc;
}

# ========================================================================================================================

sub delete {
	my ($self, $path, $params, $headers) = @_;

	# format parameters
	my $url = $self->encode_url($path, $params);

	# handle the headers
	my $hdrs = $self->set_headers($headers);

	# execute the request (POST for create)
	my $client = $self->{client};
	my $tries = $self->{tries};
	my $rc;
	for (my $i = 0; $i < $tries; $i++) {
		$client->DELETE($url, $hdrs);
		$rc = $self->parse_response();
		my $err = $self->{error};
		last if ($err->error() == Alma2::Error::ALMAERR_NO_ERROR);
	}

	# return OK = 0. !OK = 1.
	return $rc;
}

# ========================================================================================================================

1;

