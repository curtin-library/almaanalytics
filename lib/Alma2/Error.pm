
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Error;

use strict;
use warnings;
use Data::Dumper;

use Switch;
use LWP::UserAgent;
use REST::Client;
use POSIX;

use XML::LibXML;
use Encode;
use JSON;

# ========================================================================================================================

use constant ALMAERR_NO_ERROR              =>  0;
use constant ALMAERR_AUTH_ERROR            => -1;
use constant ALMAERR_CLIENT_SIDE_ERROR     => -2;
use constant ALMAERR_NOT_FOUND_ERROR       => -3;
use constant ALMAERR_INTERNAL_SERVER_ERROR => -4;
use constant ALMAERR_HOST_NOT_FOUND        => -5;
use constant ALMAERR_NOT_ACCEPTABLE        => -6;
use constant ALMAERR_HTTP_ERROR            => -100;

# ========================================================================================================================

# ==========
# Alma REST API returns the following HTTP codes
# HTTP Code	Description
# 200	Request was successful
# 204	Request was successful but no content returned e.g. for DELETE requests
# 400	Logical error - check the error message, fix your data, and try again
# 401	Unauthorized
# 404	Not found
# 406	Invalid Accept header
# 500	Server error - try your request again, and if unsuccessful, submit a support case
# 500	Client error - invalid host
# ==========

sub new {
	my ($class, $rest_client) = @_;

	my $self = {
		error => 0,
		http_code => 200,
		reason => 'OK',
		message => '',
	};
	bless $self, $class;

	$self->parse_response($rest_client);
	return $self;
}

# ========================================================================================================================

sub code {
	return $_[0]->{http_code};
}

sub error {
	return $_[0]->{error};
}

sub reason {
	return $_[0]->{reason};
}

sub message {
	return $_[0]->{message};
}

# ========================================================================================================================

sub parse_response {
	my ($self, $client) = @_;

	my $http_code = $client->responseCode();
	$self->{http_code} = $http_code;

	my $content = $client->responseContent();
	$self->{message} = $content;

	$self->{error} = ALMAERR_NO_ERROR;
	$self->{reason} = 'OK';

	switch ($http_code) {
		# request was OK
		case 200 {
			$self->{message} = '';
			return 0;
		}
		case 204 {
			$self->{message} = '';
			return 0;
		}

		# request failed. client side error. bad request. check arguments.
		case 400 {
			$self->{error} = ALMAERR_CLIENT_SIDE_ERROR;
			$self->{reason} = "HTTP 400 CLIENT SIDE ERROR (BAD REQUEST. CHECK ARGUMENTS.)";
			return -1;
		}

		# request failed. auth error
		case 401 {
			$self->{error} = ALMAERR_AUTH_ERROR;
			$self->{reason} = "HTTP 401 AUTH ERROR";
			return -1;
		}

		# request failed. not found (possibly incorrect / insufficient request information / details).
		case 404 {
			$self->{error} = ALMAERR_NOT_FOUND_ERROR;
			$self->{reason} = "HTTP 404 NOT FOUND ERROR";
			return -1;
		}

		# request failed. invalid Accept header.
		case 406 {
			$self->{error} = ALMAERR_NOT_ACCEPTABLE;
			$self->{reason} = "HTTP 406 NOT ACCEPTABLE";
			return -1;
		}

		# request failed. server-side failure / problem.
		case 500 {
			if ( $content =~ /^Can\'t connect to/ ) {
				$self->{error} = ALMAERR_HOST_NOT_FOUND;
				$self->{reason} = "HTTP 500 HOST NOT FOUND";
			} else {
				$self->{error} = ALMAERR_INTERNAL_SERVER_ERROR;
				$self->{reason} = "HTTP 500 INTERNAL SERVER ERROR";
			}
			return -1;
		}

		# unknown error
		else {
			$self->{error} = ALMAERR_HTTP_ERROR;
			$self->{reason} = "HTTP $http_code";
			return -1;
		}
	}
}

# ========================================================================================================================

1;
