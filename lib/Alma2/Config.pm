
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Config;

use strict;
use warnings;
use Data::Dumper;

our @CONSTANTS = qw( YML_FILE );
our @FUNCTIONS = qw( config );

use Exporter;
our @EXPORT_OK = qw( @CONSTANTS @FUNCTIONS );
our %EXPORT_TAGS = (
	const => [ @CONSTANTS ],
	funcs => [ @FUNCTIONS ],
);

use Alma2::Defaults;
use Alma2::Skywalker qw(:funcs :consts);
use Alma2::Config::YAML;

# ========================================================================================================================

use constant YML_FILE => Alma2::Defaults::CONFIG_DIR.'/config.yml';

# ========================================================================================================================

sub config {
	# eg Alma2::Config::config();
	my $yc = Alma2::Config::YAML->new(YML_FILE);
	return $yc;
}

# ========================================================================================================================

1;
