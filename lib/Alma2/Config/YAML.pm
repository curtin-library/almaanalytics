
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Config::YAML;

use strict;
use warnings;
use Data::Dumper;

use YAML;

use Alma2::Skywalker qw(:funcs :consts);

# ========================================================================================================================

sub new {
	my ($class, $file) = @_;

	my $conf = YAML::LoadFile($file);
	return undef if !defined $conf;

	return bless {
		config => $conf,
	}, $class;
}

sub get {
	my ($self, $path) = @_;

	my $conf = $self->{config};

	# eg Alma2::Config::config("testing.courses");
	my $list = Alma2::Skywalker::walk_path($conf, "conf.$path", "conf", Alma2::Skywalker::WALK_WANT_TRC);
	return undef if !defined $list;
	my $ref = pop @$list;

	return $ref;
}

# ========================================================================================================================

1;
