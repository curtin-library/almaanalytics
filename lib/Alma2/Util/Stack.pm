
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Util::Stack;

use strict;
use warnings;
use Data::Dumper;

# ========================================================================================================================

sub new {
	my ($class) = @_;

	my $self = {
		stack => [],
	};
	bless $class, $self;

	return $self;
}

sub push {
	my ($self, $item) = @_;
	push @{$self->{stack}}, $item;
	return;
}

sub pop {
	my ($self) = @_;
	my $item = pop @{$self->{stack}};
	return $item;
}

sub isEmpty {
	my ($self) = @_;
	return 1 if scalar @{$self->{stack}} == 0;
	return 0;
}

# ========================================================================================================================

1;
