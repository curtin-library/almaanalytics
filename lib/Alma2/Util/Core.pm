
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Util::Core;

use strict;
use warnings;
use Data::Dumper;

require Exporter;
use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
@ISA = qw(Exporter);
@EXPORT = ();
@EXPORT_OK = qw( perl split_array split_file spawn hires_sleep scrub_isbn bad_isbn check_isbn ftp_file touch cloneit max min );
%EXPORT_TAGS = (
	all => [ @EXPORT_OK ],
	isbn => [ qw(scrub_isbn bad_isbn check_isbn) ],
	ftp => [ qw(ftp_file) ],
	process => [ qw(spawn hires_sleep) ],
	perl => [ qw(perl) ],
	clone => [ qw(cloneit) ],
	maxmin => [ qw(max min) ],
);

use POSIX qw( setsid strftime );
use Fcntl;
use Net::FTP;
use File::Basename;
use Time::HiRes qw(usleep);
use Clone 'clone';

# ========================================================================================================================

my %units = (
	'k' => 1000,
	'm' => 1000*1000,
	'g' => 1000*1000*1000,
	't' => 1000*1000*1000*1000,
	'p' => 1000*1000*1000*1000*1000,
	'e' => 1000*1000*1000*1000*1000*1000,
	'K' => 1024,
	'M' => 1024*1024,
	'G' => 1024*1024*1024,
	'T' => 1024*1024*1024*1024,
	'P' => 1024*1024*1024*1024*1024,
	'E' => 1024*1024*1024*1024*1024*1024,
);

# ========================================================================================================================

sub perl {
	return "/data/alma/perl5/perlbrew/perls/perl-5.20.1/bin/perl";
}

# ========================================================================================================================

sub split_array {
	my ($asize, $ia) = @_;

	my $q = int(@$ia / $asize);
	my $r = @$ia % $asize;

	my @arrays;
	@{ $arrays[$_] } = @$ia[ ($_ * $asize) .. ((($_ + 1) * $asize) - 1) ] for 0..($q - 1);
	@{ $arrays[$q] } = @$ia[ ($q * $asize) .. (($q * $asize) + $r - 1) ] if $r != 0;
	$q++ if $r != 0;

	return @arrays;
}

# Block-size unit abbr.   Block-size units
# k                       1000
# m                       1000*1000
# g                       1000*1000*1000
# t                       1000*1000*1000*1000
# p                       1000*1000*1000*1000*1000
# e                       1000*1000*1000*1000*1000*1000
# K                       1024
# M                       1024*1024
# G                       1024*1024*1024
# T                       1024*1024*1024*1024
# P                       1024*1024*1024*1024*1024
# E                       1024*1024*1024*1024*1024*1024

sub split_file {
	my ($input, $bs) = @_;

	if ( $bs =~ m/^(\d+)([kmgtpeKMGTPE])$/ ) {
		$bs = $1 * $units{$2};
	} elsif ( $bs =~ m/^([kmgtpeKMGTPE])$/ ) {
		$bs = $units{$1};
	} elsif ( $bs =~ m/^(\d+)$/ ) {
		$bs = $1;
	} else {
		print STDERR "Alma2::Util::Core::split_file(): bad blocksize $bs";
		return;
	}

	my $name = basename($input);
	my $path = dirname($input);

	my $filesize = (stat($input))[7];
	my $nblks = int($filesize / $bs) + ($filesize % $bs ? 1 : 0);

	foreach my $skip ( 0..($nblks-1) ) {
		my $output = "$path/$name." . sprintf("%.4d", $skip + 1);
		qx/dd if=$input of=$output bs=$bs count=1 skip=$skip/;
	}
}

# ========================================================================================================================

#	# fork out a process to execute the harvesting job
#	my $cpid = fork();
#	if (!defined $cpid) {
#		die "fork($exe_dir, $prog_name, $args) failed";
#	} elsif (!$cpid) {
#		# Child cleans up descriptors
#		chdir $exe_dir or die "Can't chdir $exe_dir: $!";
#		close STDIN;
#		close STDOUT;
#		close STDERR;
#
#		# exec $dir $prog_name $args
#		exec("$perl", "$exe_dir/$prog_name", "$args");
#		exit;
#	} else {
#		# print the response
#		print_resp($cgi, $cpid, $date, $emails);
#	}

sub spawn {
	my ($logger, $shell, $dir, $prog, $args) = @_;

	# fork the child
	my $cpid = fork();
	if (!defined $cpid) {
		$logger->error("Alma2::Util::Core::spawn($shell, $dir, $prog, $args): failed to fork child", __FILE__, __LINE__);
		return undef;
	} elsif ($cpid != 0) {
		#exit if ($cpid);
		return $cpid if ($cpid);
	} else {
		# connect output to $logger's file descriptor
		$logger->connect(*STDOUT);
		$logger->connect(*STDERR);

		# announce that we have started
		$logger->info("Alma2::Util::Core::spawn($shell, $dir, $prog, $args): started process $cpid", __FILE__, __LINE__);

		# child cleans up descriptors
		chdir $dir or die "Can't chdir $dir: $!";

		# exec $shell $prog $args
		# note: $prog must be full path name and $prog must be written in the language of $shell (eg Perl for perl)
		exec("$shell", "$prog", "$args")
			or $logger->fatal("Alma2::Util::Core::spawn($shell, $dir, $prog, $args): failed to exec command", __FILE__, __LINE__);
		exit(-1);
	}
}

#sub spawn {
#	my ($logger, $shell, $dir, $prog, $args) = @_;
#
#	# fork the child
#	my $cpid = fork();
#	die "First fork ($shell, $dir, $prog, $args) failed!!" if (!defined $cpid);
#	#exit if ($cpid);
#	return $cpid if ($cpid);
#
#	# connect output to $logger's file descriptor
#	$logger->connect(*STDOUT);
#	$logger->connect(*STDERR);
#
#	# second fork detaches from parent doing the original spawning
#	$cpid = fork();
#	die "Second fork ($shell, $dir, $prog, $args) failed!!" if (!defined $cpid);
#	exit(0) if $cpid;
#
#	# announce that we have started
#	$logger->info("Alma2::Util::Core::spawn($shell, $dir, $prog, $args): started process $cpid", __FILE__, __LINE__);
#
#	# child cleans up descriptors
#	chdir $dir or die "Can't chdir $dir: $!";
#
#	# exec $shell $prog $args
#	# note: $prog must be full path name and $prog must be written in the language of $shell (eg Perl for perl)
#	exec("$shell", "$prog", "$args")
#		or $logger->fatal("Alma2::Util::Core::spawn($shell, $dir, $prog, $args): failed to execute command", __FILE__, __LINE__);
#	exit(-1);
#}

sub hires_sleep {
	my ($tts) = @_;
	my $time_left = $tts;
	while ($time_left >= 0) {
		my $nslept = usleep($time_left);
		$time_left -= $nslept;
	}
}

# ========================================================================================================================

# remove extraneous, non-ISBN characters ([^0-9xX]) from
sub scrub_isbn {
	my ($input) = @_;

	# idenitfy problem with $input
	my $isbn = $input;
	$isbn =~ tr/a-z/A-Z/;
	$isbn =~ s/[^\dX]+/ /g;
	my @tokens = split(' ', $isbn);
	$isbn = '';
	foreach my $token (@tokens) {
		$token =~ s/^\s//g;
		$token =~ s/\s$//g;
		next if bad_isbn($token);

		if ( $token =~ /^(\d{9}[\dX]{1})$/ ) {
			$isbn = $1;
			last;
		}
		if ( $token =~ /^(\d{13})$/ ) {
			$isbn = $1;
			last;
		}
	}

	return $isbn;
}

# note: isbn may or may not be pre-scrubbed.
sub bad_isbn {
	my ($isbn) = @_;

	# empty ISBN
	return 1 if (!$isbn);

	# ISBNs with invalid lengths
	return 1 if (length $isbn != 13 && length $isbn != 10);

	# case of non-ISBN-digit characters present in string
	return 1 if ($isbn =~ /[^\dXx]+/);

	# 10-digit ISBN with invalid values
	return 1 if (length $isbn == 10 and $isbn !~ /^\d{9}[\dxX]{1}$/);

	# 13-digit ISBN with invalid values
	return 1 if (length $isbn == 13 and $isbn !~ /^\d{13}$/);

	# case ISBN is 0000000000000 or 0000000000
	return 1 if ($isbn !~ /[xX]$/ and $isbn == 0);

	# check digit is not correct
	return 1 if !defined check_isbn($isbn);

	return 0;
}

sub check_isbn10 {
	my ($isbn) = @_;
	my @digits = split('', $isbn);
	my @coeffs = qw ( 10 9 8 7 6 5 4 3 2 0 );
	my $total = 0;
	for (my $i = 0; $i < 9; $i++) {
		$total += $digits[$i] * $coeffs[$i];
	}
	my $c = 11 - ($total % 11);
	return ($c == 10 ? 'X' : $c);
}

sub check_isbn13 {
	my ($isbn) = @_;
	my @digits = split('', $isbn);
	my @coeffs = qw( 1 3 1 3 1 3 1 3 1 3 1 3 0 );
	my $total = 0;
	my $i;
	for ($i = 0; $i < 12; $i++) {
		$total += $digits[$i] * $coeffs[$i];
	}
	my $c = 10 - ($total % 10);
	return $c;
}

sub check_isbn {
	my ($isbn) = @_;
	return check_isbn10($isbn) if ( length($isbn) == 10 );
	return check_isbn13($isbn) if ( length($isbn) == 13 );
	return undef;
}

# ========================================================================================================================

sub ftp_file {
	my ($host, $user, $password, $dir, $file) = @_;

	# Create object to connect to host
	my $ftp = Net::FTP->new($host);
	if ( !defined $ftp ) {
		# error, $ftp->message;
		return (0, $ftp->message);
	}

	# provide creds to host
	if ( !$ftp->login($user, $password) ) {
		# error, $ftp->message;
		return (0, $ftp->message);
	}

	# tell host we are sending binary
	$ftp->binary();

	# change directory to directory
	if ( !$ftp->cwd($dir) ) {
		# error, $ftp->message;
		return (0, $ftp->message);
	}

	# drop file into place
	if ( !$ftp->put($file) ) {
		# error, $ftp->message;
		return (0, $ftp->message);
	}

	# say goodbye
	$ftp->quit();

	# we're done
	return (1, '');
}

# ========================================================================================================================

sub touch {
	my ($file, $umask) = @_;

	my $omask = umask;
		$umask = 0002 if !defined $umask;
		umask $umask;
		sysopen(FILE, $file, O_RDWR|O_CREAT);
		close FILE;
	umask $omask;
}

# ========================================================================================================================

sub cloneit {
	my ($o) = @_;

	my $o_cloned = clone($o);
	if ( ref($o_cloned) eq 'HASH' ) {
		foreach my $key (keys %$o_cloned) {
			if ($key =~ /^HASH\(0x/) {
				delete $o_cloned->{$key};
			}
		}
	}
	return $o_cloned;
}

# ========================================================================================================================

sub max {
	my ($a, $b) = @_;
	return ($a > $b ? $a : $b);
	#return [ $a, $b ] -> [ $a <= $b ];
}

sub min {
	my ($a, $b) = @_;
	return ($a < $b ? $a : $b);
	#return [ $a, $b ] -> [ $b <= $a ];
}

# ========================================================================================================================

1;
