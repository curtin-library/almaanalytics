
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Util::DateTime;

use strict;
use warnings;
use Data::Dumper;

use Date::Calc;
use Date::Manip;
use POSIX qw/strftime/;
use Time::HiRes qw(gettimeofday);

# ========================================================================================================================

use constant MICROSECOND => 1000000;
use constant MILLISECOND => 1000;

# ========================================================================================================================

sub new {
	my ($class, $time) = @_;

	my $self = {};
	bless $self, $class;

	my $reftype = ref($time);
	if (!$reftype) {
		$self->{time} = ( defined $time ? $time : time() );
		$self->{usecs} = 0;
	} elsif ($reftype eq 'ARRAY') {
		$self->{time} = $time->[0];
		$self->{usecs} = $time->[1];
	} else {
		$self->now();
	}

	return $self;
}

sub duplicate {
	my ($self) = @_;
	my $obj = Alma2::Util::DateTime->new();
	$obj->{time} = $self->{time};
	$obj->{usecs} = $self->{usecs};
	return $obj;
}

# ========================================================================================================================

sub set {
	my ($self, %atime) = @_;

	# time in seconds (an integer)
	if (exists $atime{time}) {
		$self->{time} = $atime{time};
	# time as a string
	} elsif (exists $atime{string}) {
		$self->{time} = UnixDate($atime{string}, '%s');
	# time as an array [secs, microsecs]
	} elsif (exists $atime{hires}) {
		my $time = $atime{hires}->[0];
		my $usecs = $atime{hires}->[1];
		$self->{time} = $time;
		$self->{usecs} = $usecs;
	# nothing specified - defaults to now
	} else {
		$self->now();
	}
	return $self;
}

# ========================================================================================================================

sub now {
	my ($self) = @_;

	my @tod = gettimeofday();
	$self->{time} = $tod[0];
	$self->{usecs} = $tod[1];

	return $self;
}

sub time {
	my ($self) = @_;
	return $self->{time};
}

sub usecs {
	my ($self) = @_;
	return $self->{usecs};
}

sub float {
	my ($self) = @_;
	return ( $self->{time} + ($self->{usecs} / MICROSECOND) );
}

sub datestamp {
	my ($self, $fmts) = @_;
	my @lt = localtime($self->{time});
	return strftime('%Y/%m/%d', @lt) if !defined $fmts;
	return strftime('%Y/%m/%d', @lt) if !$fmts;
	return strftime($fmts,      @lt);
}

sub timestamp {
	my ($self, $fmts) = @_;
	my @lt = localtime($self->{time});
	return strftime('%H:%M:%S', @lt) if !defined $fmts;
	return strftime('%H:%M:%S', @lt) if !$fmts;
	return strftime($fmts,      @lt);
}

sub datetimestamp {
	my ($self, $fmts) = @_;
	my @lt = localtime($self->{time});
	return strftime('%Y-%m-%d %H:%M:%S', @lt) if !defined $fmts;
	return strftime('%Y-%m-%d %H:%M:%S', @lt) if !$fmts;
	return strftime($fmts,      @lt);
}

sub msecstamp {
	my ($self) = @_;
	return sprintf('%03d', $self->{usecs} / MILLISECOND);
}

sub usecstamp {
	my ($self) = @_;
	return sprintf('%06d', $self->{usecs});
}

# ========================================================================================================================

# semantics $date1->cmp($date2) : $date1 cmp $date2
sub _cmp {
	my ($self, $date) = @_;

	use bignum;
	my $t1 = ($self->{time} * MICROSECOND) + $self->{usecs};
	my $t2 = ($date->{time} * MICROSECOND) + $date->{usecs};
	my $diff = ($t1 - $t2);
	my $_cmp_;
	if ($diff < 0) {
		$_cmp_ = -1;
	} elsif ($diff == 0) {
		$_cmp_ = 0;
	} else {
		$_cmp_ = 1;
	}
	no bignum;

	return $_cmp_;
}

sub lt {
	my ($self, $date) = @_;
	return ($self->_cmp($date) < 0);
}
sub le {
	my ($self, $date) = @_;
	return ($self->_cmp($date) <= 0);
}

sub ne {
	my ($self, $date) = @_;
	return ($self->_cmp($date) != 0);
}

sub eq {
	my ($self, $date) = @_;
	return ($self->_cmp($date) == 0);
}

sub ge {
	my ($self, $date) = @_;
	return ($self->_cmp($date) >= 0);
}

sub gt {
	my ($self, $date) = @_;
	return ($self->_cmp($date) > 0);
}

# ========================================================================================================================

# As a caution, don't add or sub years. with Date::Manip functions one year != 365 days,
# but one year = 365.2425 days. You will be adding on 31556952-31536000=20952 seconds,
# or 5 hours 49 minutes 12 seconds to your datetime. Most likely not what you wanted.

# add : { one | n } { day | days | month | months | week | weeks | year | years }
# sub : { one | n } { day | days | month | months | week | weeks | year | years }

sub compute_delta {
	my ($self, $ofs) = @_;

	# use Date::Manip::Delta to parse the offset
	my $delta = new Date::Manip::Delta();
	$delta->parse($ofs);

	# turn the delta/offset into seconds
	my $secs = $delta->printf('%syM')
	         + $delta->printf('%swd')
	         + $delta->printf('%shm')
	         + $delta->printf('%sss');

	# return seconds
	return $secs;
}

sub add {
	my ($self, $offset) = @_;

	use bigint;
		# Date::Manip::Calc doesn't seem to work as advertised so we roll our own.
		my $secs = $self->compute_delta($offset);

		# add in microseconds
		$secs *= MICROSECOND;
		my $time = ($self->{time} * MICROSECOND) + $self->{usecs};
		$time = $time + $secs;

		# convert into [secs, usecs]
		$self->{time} = int($time / MICROSECOND);
		$self->{usecs} = ($time % MICROSECOND);
	no bigint;

	return $self;
}

sub sub {
	my ($self, $offset) = @_;

	use bigint;
		# Date::Manip::Calc doesn't seem to work as advertised so we roll our own.
		my $secs = $self->compute_delta($offset);

		# subtract in microseconds
		$secs *= MICROSECOND;
		my $time = ($self->{time} * MICROSECOND) + $self->{usecs};
		$time = $time - $secs;

		# convert into [secs, usces]
		$self->{time} = int($time / MICROSECOND);
		$self->{usecs} = ($time % MICROSECOND);
	no bigint;

	return $self;
}

# ========================================================================================================================

# note: diff is in microseconds. do int($t->diff($a) / Alma2::Util::DateTime::MICROSECOND) to get seconds

sub diff {
	my ($self, $t) = @_;

	use bigint;
		my $tself = ($self->{time} * MICROSECOND) + $self->{usecs};
		my $tt = ($t->{time} * MICROSECOND) + $t->{usecs};
		my $tdiff = $tself - $tt;
	no bigint;

	return $tdiff;
}

# ========================================================================================================================

# deconstructs a DateTime object into UTC year, month, day, hour, minute, second, GMT offset and seconds.

sub deconstruct {
	my ($self, $res) = @_;

	my $dts = $self->datetimestamp("%Y %m %d %H %M %S %z %Z");
	my ($yr, $mo, $dy, $hr, $mi, $ss, $z, $zn) = split(' ', $dts);
	my $utc_secs = $self->{time};
	my $usecs = $self->{usecs};

	my ($sign, $HH, $MM) = ($z =~ /^([+-])(\d\d)(\d\d)$/);
	my $ofs = (3600 * $HH) + (60 * $MM);
	$ofs *= ($sign eq '+' ? 1 : -1);

	my @lt = localtime($self->{time});
	my $dls = $lt[8];
	my $yday = $lt[7];
	my $wday = $lt[6];

	if ( ref($res) eq 'ARRAY' ) {
		@$res = ($ss, $mi, $$hr, $dy, $mo, $yr, $wday, $yday, $dls, $usecs, $ofs, $utc_secs, $zn);
	} elsif ( ref($res) eq 'HASH' ) {
		%$res = (
			second => $ss,
			minute => $mi,
			hour => $hr,
			day => $dy,
			month => $mo,
			year => $yr,
			wday => $wday,
			yday => $yday,
			daylight => $dls,
			usecs => $usecs,
			offset => $ofs,
			utctime => $utc_secs,
			zone => $zn,
		);
	} else {
		die "Alma2::Util::DateTime::deconstruct() invalid reference type '" . ref($res) . "' provided. Aborting.";
	}

	return;
}

# ========================================================================================================================

1;
