
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Util::DateTimeUtil;

use strict;
use warnings;
use Data::Dumper;

use DateTime;
use POSIX qw(strftime mktime);

use Alma2::Util::DateTime;

# ========================================================================================================================

require Exporter;

our @ISA = qw(Exporter);
our @EXPORT_OK = qw( getTimeX DateTimeFromDMY cmpAlmaDateStr );
our %EXPORT_TAGS = ( time => [qw( getTimeX DateTimeFromDMY cmpAlmaDateStr )], );

# ========================================================================================================================

# returns formatted timestamp and milliseconds

sub getTimeX {
	my ($fmts, $secs, $usecs) = @_;
	my $time = Alma2::Util::DateTime->new($secs);
	my $s = ($fmts ? $time->timestamp($fmts) : '');
	my $msecs = sprintf("%03d", $usecs / 1000);
	return ($s, $msecs);
}

# gets date and time components from an Alma XML date string (example: 21/06/2014 16:50:00 WST)

sub DateTimeFromDMY {
	my ($dts) = @_;
	my ($date, $time, $zone) = split(' ', $dts);
	my ($dd, $mm, $yyyy) = split('/', $date);
	my ($hh, $mi, $ss) = split(':', $time);
	my $t = DateTime->new(
		nanosecond => 0,
		second => $ss,
		minute => $mi,
		hour => $hh,
		day => $dd,
		month => $mm,
		year => $yyyy
	);
	return $t;
}

# compares two Alma XML date strings (example: 21/06/2014 16:50:00 WST)

sub cmpAlmaDateStr {
	my ($d1, $d2) = @_;
	my $t1 = DateTimeFromDMY($d1);
	my $t2 = DateTimeFromDMY($d2);
	return DateTime->compare_ignore_floating($t1, $t2);
}

# ========================================================================================================================

1;
