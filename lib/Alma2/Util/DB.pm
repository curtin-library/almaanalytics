
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Util::DB;

use strict;
use warnings;
use Data::Dumper;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw( getdbh );
our %EXPORT_TAGS = (
	dbh => [ qw( getdbh ) ],
);

use DBI;
use Alma2::Defaults;

# ========================================================================================================================

use constant ORACLE_HOME => Alma2::Defaults::ORACLE_HOME;
use constant ORACLE_PATH => Alma2::Defaults::ORACLE_PATH;

# ========================================================================================================================

sub _get_mysql {
	my (%args) = @_;

	eval {
		require DBD::mysql;
	} or die "Alma2::Util::DB getdbh._get_mysql() You do not have DBD::mysql installed. Please install it from CPAN before using getdbh().";

	my ($db, $host, $port, $uid, $pwd) = ($args{db}, $args{host}, $args{port}, $args{uid}, $args{pwd});
	my $dbh = DBI->connect("DBI:mysql:database=$db;host=$host:port=$port", $uid, $pwd) or die $DBI::errstr;

	return $dbh;
}

sub _get_ora {
	my (%args) = @_;

	eval {
		require DBD::Oracle;
	} or die "Alma2::Util::DB getdbh._get_mysql() You do not have DBD::Oracle installed. Please install it from CPAN before using getdbh().";

	my ($sid, $uid, $pwd, $aleph_utf) = ($args{sid}, $args{uid}, $args{pwd}, $args{al32utf8});

	$ENV{ORACLE_SID} = $sid;
	$ENV{ORACLE_HOME} = ORACLE_HOME;
	$ENV{PATH} = $ENV{PATH}.":".ORACLE_PATH;

	if ($aleph_utf) {
		$ENV{NLS_LANG} = 'AMERICAN_AMERICA.AL32UTF8';
		$ENV{NLS_NCHAR} = 'AL32UTF8';
	}

	my $dbh = DBI->connect("DBI:Oracle:$sid", $uid, $pwd,
		{ PrintError => 1,
		  RaiseError => 0,
		  AutoCommit => 1 }) || die $DBI::errstr;

	$dbh->{'LongReadLen'} = 1000000;
	$dbh->{'LongTruncOk'} = 1;

	return $dbh;
}


#
# argument keys: MySQL:
#                db = name of MySQL database
#                uid = MySQL user id
#                pwd = MySQL user password
#                host = MySQL server host name or address as configured in MySQL admin database table
#                port = MySQL server port number
#
# argument keys: Oracle
#                sid = SID of Oracle database. should be configured in the local client tnsnames.ora file
#                uid = Oracle user id
#                pwd = Oracle user password
#                al32utf8 = 1 : set NLS_LANG and NLS_NCHAR. 0 : otherwise
#
# Examples:
# my $dbh = getdbh('ora', sid => 'LISPRD', uid => 'lis1', pwd => 'lis1', al32utf8 => 0);
# my $dbh = getdbh('mysql', db => 'reswebtools', host => '134.7.94.15', port => '3306', uid => 'resweb', pwd => 'resweb');
#

sub getdbh {
	my ($which, %args) = @_;

	if ($which eq 'ora') {
		return _get_ora(%args);
	}
	elsif ($which eq 'mysql') {
		return _get_mysql(%args);
	}
	else {
		return undef;
	}
}

# ========================================================================================================================

1;
