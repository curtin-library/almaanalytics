
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Skywalker;

use strict;
use warnings;
use Data::Dumper;

use Exporter;
our @FUNCTIONS = qw( walk_path add_path remove_path set_value );
our @CONSTANTS = qw ( WALK_WANT_REF WALK_WANT_VAL WALK_WANT_TRC );
our @EXPORT_OK = qw( @FUNCTIONS @CONSTANTS );
our %EXPORT_TAGS = (
	funcs => [ @FUNCTIONS ],
	consts => [ @CONSTANTS ],
);

# ========================================================================================================================

use constant WALK_WANT_REF => 1;
use constant WALK_WANT_VAL => 2;
use constant WALK_WANT_TRC => 3;

# ========================================================================================================================

sub _do_rwalk {
	my ($struct, $stack, $tokens) = @_;

	return $struct if !scalar @$tokens;

	my $ckey = shift @$tokens;

	if ( $ckey =~ /^(\w+)\[(\d+)\]$/ ) {
		my ($key, $idx) = ($1, $2);

		# first conside if $struct->{$key}->[$idx] is a non-hash node
		if ( !ref($struct->{$key}->[$idx]) ) {
			# this means that $struct does not map the path in structure
			return 0 if scalar @$tokens > 0;

			push @$stack, $struct;
			$struct = $struct->{$key};
			push @$stack, $struct;
			$struct = $struct->[$idx];

			return 1;
		} else {
			return 0 if ref($struct) ne 'HASH';
			return 0 if !defined $struct->{$key};
			return 0 if !defined $struct->{$key}->[$idx];

			push @$stack, $struct;
			$struct = $struct->{$key};
			push @$stack, $struct;
			$struct = $struct->[$idx];

			if (scalar @$tokens > 0) {
				return _do_rwalk($struct, $stack, $tokens);
			} else {
				push @$stack, $struct;
				return 1;
			}
		}
	} elsif ( $ckey =~ /^\w+$/ ) {
		return 0 if ref($struct) ne 'HASH';
		return 0 if !defined $struct->{$ckey};

		if (scalar @$tokens > 0) {
			push @$stack, $struct;
			$struct = $struct->{$ckey};
			return _do_rwalk($struct, $stack, $tokens);
		} else {
			push @$stack, $struct;
			$struct = $struct->{$ckey};
			push @$stack, $struct;
			return 1;
		}

		return 1;
	} else {
		return 0;
	}
}

# walk_path : returns a stack of references up to leaf's parent node
# for a hash-element : last element on stack is leaf's parent
# for an array element : last element on stack is the parent array
# $want : 1 - want ref of path leaf.
#         2 - want value of leaf referent.
#         3 - want stack up to path leaf.

sub walk_path {
	my ($struct, $path, $root, $want) = @_;

	my @stack = ();
	my @tokens = split(/\./, $path);

	# first check for consistency
	return undef if $tokens[0] ne $root;

	# check that no token is an integer
	foreach my $token (@tokens) {
		return undef if ($token =~ /^\d+$/);
	}

	# walk the tree recursively
	shift @tokens;
	return undef if !_do_rwalk($struct, \@stack, \@tokens);

	if ( $want == WALK_WANT_TRC ) {
		return \@stack;
	} elsif ( $want == WALK_WANT_REF ) {
		pop @stack;
		return pop @stack;
	} else {
		return pop @stack;
	}
}

# ========================================================================================================================

sub is_array {
	my ($nodes, $i) = @_;
	return 1 if $nodes->[$i] =~ /[a-zA-Z0-9_]+\[\d+\]/;
	return 0;
}

sub is_hash {
	my ($nodes, $i) = @_;
	return 0 if $nodes->[$i] =~ /[a-zA-Z0-9_]+\[\d+\]/;
	my $n = @$nodes;
	return ($i + 1 < $n);
}

sub add_path {
	my ($struct, $path, $root, $force) = @_;

	# tokenise the path (separator is always '.')
	$path =~ s/\s+//g;
	my @nodes = split(/\./, $path);

	# first token/node must always be 'course'
	return undef if $nodes[0] !~ /^$root/i;

	my $ref = $struct;
	my @stack;
	push @stack, $ref;
	shift @nodes;
	my $num = @nodes;
	my $i;
	for ($i = 0; $i < $num; $i++) {
		my $node = $nodes[$i];
		#$ref = pop @stack if !defined $ref;
		if ( is_array(\@nodes, $i) ) {
			# $node is an ARRAY-element
			my ($anode, $n) = ($node =~ /([a-zA-Z0-9_]+)\[(\d+)\]/);
			if ( exists $ref->{$anode} ) {
				# $node exists...
				if ( defined $ref->{$anode} ) {
					# but the path 'thinks' it is not an ARRAY
					if ( ref($ref->{$anode}) ne 'ARRAY' ) {
						# we need to flag this error with undef
						return undef;
					}
				} else {
					$ref->{$anode} = [];
				}

				push @stack, $ref;
				$ref = $ref->{$anode};
				$ref->[$n] = {} if $i + 1 < $num  and !defined $ref->[$n];
				$ref->[$n] = '' if $i + 1 == $num  and !defined $ref->[$n];

				push @stack, $ref;
				$ref = $ref->[$n];
			} else {
				# $node doesn't exist, so instantiate $ref->{$node}->[$n]
				$ref->{$anode} = [];
				$ref->{$anode}->[$n] = {} if ($i + 1 < $num);
				$ref->{$anode}->[$n] = '' if ($i + 1 >= $num);

				push @stack, $ref;
				$ref = $ref->{$anode};

				push @stack, $ref;
				$ref = $ref->[$n];
			}
		} elsif ( is_hash(\@nodes, $i) ) {
			# $node is a HASH
			if ( exists $ref->{$node} ) {
				if ( !defined $ref->{$node} ) {
					$ref->{$node} = {} if ($i + 1 < $num);
					$ref->{$node} = '' if ($i + 1 >= $num);
					push @stack, $ref;
					$ref = $ref->{$node} if (ref($ref->{$node}) eq 'HASH');
				} else {
					if ( $force && !ref($ref->{$node}) && $i + 1 < $num ) {
						# allow for prior partial path creation by overwriting what was there with a hash
						$ref->{$node} = {};
					}

					push @stack, $ref;
					$ref = $ref->{$node};
				}
			} else {
				# $node doesn't exist so instantiate $ref->{$node}
				$ref->{$node} = {} if ($i + 1 < $num);
				$ref->{$node} = '' if ($i + 1 >= $num);
				push @stack, $ref;
				$ref = $ref->{$node};
			}
		} else {
			if ( !exists $ref->{$node} ) {
				# $node doesn't exist so instantiate $ref->{$node}
				$ref->{$node} = {} if ($i + 1 < $num);
				$ref->{$node} = '' if ($i + 1 >= $num);
			}

			# move to $nodes[$i];
			push @stack, $ref;
			$ref = $ref->{$node};
		}
	}

	$ref = pop @stack if !defined $ref;
	return $ref;
}

# ========================================================================================================================

sub remove_path {
	my ($struct, $path, $root) = @_;

	return undef if !$root || !$path || !defined $struct;
	return $struct if $path =~ /^$root$/;

	# tokenise the path (separator is always '.')
	$path =~ s/\s+//g;
	my @nodes = split(/\./, $path);

	# first token/node must always be 'course'
	return undef if $nodes[0] !~ /^$root/i;

	my $ref = $struct;

	shift @nodes;
	my $num = @nodes;
	my $i;
	for ($i = 0; $i < $num; $i++) {
		my $node = $nodes[$i];
		if ( $node =~ /([a-zA-Z0-9_]+)\[(\d+)\]/ ) {
			my ($anode, $n) = ($1, $2);

			# if these are true, then most likely the caller is mistaken about the structure of $struct
			return undef if !ref($ref) and $i + 1 < $num;
			return undef if !exists $ref->{$anode};
			return undef if !defined $ref->{$anode};

			# move down one level
			$ref = $ref->{$anode};
			return undef if !exists $ref->[$n];

			if (defined $ref->[$n] and $i + 1 < $num) {
				$ref = $ref->[$n];
			} else {
				if ($i + 1 >= $num){
					my $tmp = $ref->[$n];
					delete $ref->[$n];
					return $tmp;
				} else {
					last;
				}
			}
		} else {
			return undef if !exists $ref->{$node};

			if (defined $ref->{$node} and $i + 1 < $num) {
				$ref = $ref->{$node};
			} else {
				if ($i + 1 >= $num){
					my $tmp = $ref->{$node};
					delete $ref->{$node};
					return $tmp;
				} else {
					last;
				}
			}
		}
	}

	return undef;
}

# ========================================================================================================================

sub set_value {
	my ($struct, $path, $root, $value) = @_;

	return undef if ref($value);

	# example paths:
	# a.b.c.d.e.f.g[0]
	# a.b.c.d.e.f.h[0].i
	# a.b.c.d.e.f.j[0].k.l
	# a.b.c.d.e.f.m

	my @tokens = split(/\./, $path);
	my $leaf = pop @tokens;

	my $last = pop @tokens;
	push @tokens, $last;
	my $parpath = join('.', @tokens);

	my $ref = walk_path($struct, $parpath, $root, WALK_WANT_REF);
	return undef if (!defined !ref);

	my $node;
	if ( $last =~ /^(\w+)\[(\d+)\]$/ ) {
		my ($key, $idx) = ($1, $2);
		$node = $ref->[$idx];
	} else {
		$node = $ref->{$last};
	}

	if ( $leaf =~ /^(\w+)\[(\d+)\]$/ ) {
		my ($key, $idx) = ($1, $2);

		return undef if ref($node) ne 'HASH';
		return undef if !defined $node->{$key};
		return undef if ref($node->{$key}) ne 'ARRAY';
		return undef if !defined $node->{$key}->[$idx];

		$node->{$key}->[$idx] = $value;
		return $node->{$key}->[$idx];
	} elsif ( $leaf =~ /^\w+$/ ) {
		return undef if ref($node) ne 'HASH';
		return undef if !defined $node->{$leaf};

		$node->{$leaf} = $value;
		return $node->{$leaf};
	} else {
		return undef;
	}
}

# ========================================================================================================================

sub get_value {
	my ($struct, $path, $root, $value) = @_;

	return undef if ref($value);

	# example paths:
	# a.b.c.d.e.f.g[0]
	# a.b.c.d.e.f.h[0].i
	# a.b.c.d.e.f.j[0].k.l
	# a.b.c.d.e.f.m

	my @tokens = split(/\./, $path);
	my $leaf = pop @tokens;

	my $last = pop @tokens;
	push @tokens, $last;
	my $parpath = join('.', @tokens);

	my $ref = walk_path($struct, $parpath, $root, WALK_WANT_REF);
	return undef if (!defined !ref);

	my $node;
	if ( $last =~ /^(\w+)\[(\d+)\]$/ ) {
		my ($key, $idx) = ($1, $2);
		$node = $ref->[$idx];
	} else {
		$node = $ref->{$last};
	}

	if ( $leaf =~ /^(\w+)\[(\d+)\]$/ ) {
		my ($key, $idx) = ($1, $2);

		return undef if ref($node) ne 'HASH';
		return undef if !defined $node->{$key};
		return undef if ref($node->{$key}) ne 'ARRAY';
		return undef if !defined $node->{$key}->[$idx];

		return $node->{$key}->[$idx];
	} elsif ( $leaf =~ /^\w+$/ ) {
		return undef if ref($node) ne 'HASH';
		return undef if !defined $node->{$leaf};

		return $node->{$leaf};
	} else {
		return undef;
	}

}

# ========================================================================================================================

1;

=pod

=head2 METHODS

=head3 C<my $ref = Alma2::Skywalker::walk_path($struct, $path, $prefix, $want);>
=head3 C<my $list = Alma2::Skywalker::walk_path($struct, 'a.b.c.d[1].e.f', 'a', 0);>

Does a directional walk along a path in a Perl structure.
$struct: the object to traverse
$path:   must include the name prefix. the head must match $prefix
$prefix: the path name prefix. must same as the head of the path
$want:   what you want
         specify $want == 1 if you want the path leaf node's parent.
                 $want == 3 if you want a path trace-stack.
                 $want == 2 if you want the leaf node of the path (array/hash reference or a scalar).

=head3 item add_path($struct, $path, $prefix, $force);

Create/instantiate a path in a Perl hash-array structure.
Typically load a string that represents a structure path from a configuration
file and employ add_path() to auto-instantiate the path in the structure.
$struct: the object to operate on
$path:   must include the name prefix. the head must match $prefix
$prefix: the path name prefix. must same as the head of the path
$force:  force add_path to overwrite anything that may already exist (be very careful with this).
         specify $force == 1 if you want to replace what may already exist
                 $force == 0 if you want to avoid replacing what may already exist

=head3 item remove_path($struct, 'a.b.c.d[1].e.f', 'a');

Remove the leaf node indicated the path in a Perl hash-array structure.
The entire subtree will be removed from the structure and returned to the caller.

=cut
