
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Analytics::Client;

use strict;
use warnings;
use Data::Dumper;

use POSIX;

use Alma2::Defaults;
use Alma2::REST::Client;
use Alma2::Analytics::Statement;

# ========================================================================================================================

use constant ANALYTICS_ROOT_DIR => Alma2::Defaults::ALMA_HOME . "/analytics";
use constant ANALYTICS_CONFIG_DIR => ANALYTICS_ROOT_DIR . "/../conf";
use constant ANALYTICS_FILTER_XMLNS => 'xmlns:saw="com.siebel.analytics.web/report/v1.1" '	.
	'xmlns:sawx="com.siebel.analytics.web/expression/v1.1" ' .
	'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' .
	'xmlns:xsd="http://www.w3.org/2001/XMLSchema"';

# ========================================================================================================================

#
# Usage:
# my $config = Alma2::REST::Config->config('production','analytics');	# { host => 'https://api-ap.hosted.exlibrisgroup.com', apikey => '...', ... }
# my $analytics = Alma2::Analytics->new($config);
#

sub new {
	my ($class, $conf, %options) = @_;

	# create an Alma2::REST::Client
	my $client = Alma2::REST::Client->new($conf, %options);
	my $this = bless {
		# Alma REST client
		_client => $client,

		# generic members
		# basepath and root example: basepath = /almaws/v1. root = /analytics/reports. prefix = /almaws/v1/analytics/reports.
		_prefix => $conf->{root},
		_reportPath => undef,
		_filter => undef,

		# options
		_skip_column0 => 1,
		_columns => undef,

		# errors
		_error => 0,
		_errstr => '',
	}, $class;
	return $this;
}

# ================================================================================================================

sub client () {
	$_[0]->{_client};
}

sub prefix () {
	$_[0]->{_prefix};
}

sub reportPath () {
	$_[0]->{_path};
}

sub filter () {
	$_[0]->{_filter};
}

sub skipColumn0 () {
	$_[0]->{_skip_column0};
}

sub options () {
	my ($this) = @_;
	my %opts = (
		_skip_column0 => $this->{_skip_column0},
		_columns => $this->{_columns},
	);
	return %opts;
}

# ================================================================================================================

sub setOptions {
	my ($this, %options) = @_;
	foreach my $key (keys %options) {
		$this->{$key} = $options{$key};
	}
}

# ================================================================================================================

sub is_error {
	return ( $_[0]->{_error} ? 1 : 0 );
}

sub error {
	return $_[0]->{_error};
}

sub errstr () {
	return $_[0]->{_errstr};
}

sub clearError {
	my ($this) = @_;
	$this->{_error} = 0;
	$this->{_errstr} = '';
}

#sub setError {
#	my ($this, $error) = @_;
#	$this->{_error} = 1;
#	$this->{_errstr} = $error->error() . "\n" . $error->error_detail();
#}

sub setError {
	my $this = shift;
	if (@_ >= 2) {
		return if $_[0] =~ /\D/;
		$this->{_error} = $_[0];
		$this->{_errstr} = $_[1];
	} else {
		if ( ref $_[0] ) {
			my $arg = shift;
			if ( UNIVERSAL::isa($arg, 'Alma2::REST::Error') ) {
				my $errors = $arg->get();
				my $err = $errors->[0];
				$this->{_error} = $err->{code};
				$this->{_errstr} = $err->{message};
			} elsif ( UNIVERSAL::isa($arg, 'Alma2::Error') ) {
				$this->{_error} = $arg->error();
				$this->{_errstr} = $arg->message();
			} else {
				die "Alma2::Analytics->setError(): Cannot set unknown error class";
			}
		} else {
			die "Alma2::Analytics->setError(): Cannot set unknown error class";
		}
	}
}

# ================================================================================================================

sub prepare {
	my ($this, $path, $row_tmpl) = @_;
	my $sth = Alma2::Analytics::Statement->new($this, $path, $row_tmpl);
	return $sth;
}

sub disconnect () {
	my ($this) = @_;

	# generic members
	$this->{_prefix}  = '';
	$this->{_reportPath} = '';
	$this->{_filter} = undef;

	# options
	$this->{_skip_column0} = 1;
	$this->{_columns} = undef;

	# errors
	$this->{_error} = 0;
	$this->{_errstr} = '';
}

# ========================================================================================================================

1;
