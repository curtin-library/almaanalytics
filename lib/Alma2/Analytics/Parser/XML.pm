
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Analytics::Parser::XML;

use strict;
use warnings;
use Data::Dumper;

use XML::LibXML;

use Alma2::Analytics::RowStruct;
use Alma2::XPC;

# ========================================================================================================================

sub new {
	my ($class) = @_;
	return bless {}, $class;
}

# ========================================================================================================================

sub parse_response {
	my ($self, $xml, $rowtmpl, $defs) = @_;

	# ========================================
	# rc = 0 means OK.

	my $results = {
		rc => 0,
		has_more => 0,
		token => undef,
		set => undef,
		defs => undef,
	};

	# ========================================
	# parse the header

	my @columns;
	my %lutcols;
	my $has_more;
	my $row_element;
	my $token;

    # <rowset:rowset xmlns:rowset="urn:schemas-microsoft-com:xml-analysis:rowset" xmlns="urn:schemas-microsoft-com:xml-analysis:rowset">
    #   <xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:saw-sql="urn:saw-sql" targetNamespace="urn:schemas-microsoft-com:xml-analysis:rowset">
	my $dom = XML::LibXML->load_xml(string => $xml);
	my $xpath = XML::LibXML::XPathContext->new();
	$xpath->registerNs('x', 'urn:schemas-microsoft-com:xml-analysis:rowset');
	$xpath->registerNs('xsd', 'http://www.w3.org/2001/XMLSchema');
	my $xpc = Alma2::XPC->new($xpath);

	# resumption token!
	my $nodes = $xpc->find_arrayref($dom, '//QueryResult/ResumptionToken');
	if ( scalar @$nodes ) {
		$results->{token} = $nodes->[0]->textContent;
	}

	# end of query?
	$nodes = $xpc->find_arrayref($dom, '//QueryResult/IsFinished');
	if ( scalar @$nodes ) {
		$results->{has_more} = ( $nodes->[0]->textContent eq 'true' ? 0 : 1 );
	}

	# row-element and column definitions. these will only occur once and together (in the first block returned by the api)
	$nodes = $xpc->find_arrayref($dom, '//QueryResult/ResultXml//xsd:complexType');
	if ( scalar @$nodes ) {
		# get the XML tagname of the row element
		my $node = $nodes->[0];
		$row_element = $node->getAttribute('name');

		# save the column definitions
		$nodes = $xpc->find_arrayref($node, 'xsd:sequence/xsd:element');
		if ( scalar @$nodes ) {
			my $x = 0;
			@columns = map { {
				name => $_->getAttribute("name"),
				type => $_->getAttributeNS("urn:saw-sql", "type"),
				pos => $x++,
			} } @$nodes;
			$x = 0;
			%lutcols = map { $_->{name} => $x++, } @columns;
			$results->{defs}->{row_element} = $row_element;
			$results->{defs}->{columns} = \@columns;
			$results->{defs}->{lutcols} = \%lutcols;
		}
	} else {
		# we need to set the definitions to what was previously remembered
		# as the analytics rest api does not return the definitions from
		# the second continuation block onwards
		if ( $defs ) {
			$row_element = $defs->{row_element};
			@columns = @{$defs->{columns}};
			%lutcols = %{$defs->{lutcols}};
		}
	}

	# if $row_element is undefined at this point then something isn't right with the XML
	return $results if !defined $row_element;

	# ========================================
	# parse the body.
	# by default, oracle analytics does not return any column which is null,
	# so special checking is needed to skip these empty columns

	my @resultSet = ();
	my $rows = $xpc->find_arrayref($dom, "/report/QueryResult/ResultXml/x:rowset/x:$row_element");
	foreach my $row (@$rows) {
		my @row;
		foreach my $coldef (@columns) {
			my $colname = $coldef->{name};
			my $type = $coldef->{type};
			my $x = $lutcols{$colname};
			$row[$x] = default_value($type);

			my @col = $row->getChildrenByTagNameNS('*', $colname);
			next if !@col;

			$row[$x] = $col[0]->textContent;
		}
		shift @row if $rowtmpl->skipColumn0();
		$row = $rowtmpl->toStruct(\@row);
		push @resultSet, $row;
	}

	# no values could mean retry with token | end of data | error
	$results->{set} = ( scalar @resultSet ? \@resultSet : undef );

	# ========================================
	# return the results

	return $results;
}

sub default_value {
	my ($type) = @_;
	return '' if $type eq 'varchar';
	return 0 if $type eq 'integer' or $type eq 'number';
	return '0000-00-00' if $type eq 'date';
	return '0000-00-00 00:00:00' if $type eq 'datetime';
	return '0000-00-00 00:00:00.00' if $type eq 'timestamp';
	return '';
}

# ========================================================================================================================

1;
