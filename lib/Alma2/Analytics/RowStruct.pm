
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Analytics::RowStruct;

use strict;
use warnings;
use Data::Dumper;

# ========================================================================================================================

sub new {
	my ($class, $structure) = @_;

	my @array = ();
	push @array, @$structure;
	my $self = {
		cols => \@array,
		skip => 1,
	};
	bless $self, $class;
	return $self;
}

sub skipColumn0 {
	$_[0]->{skip};
}

sub toStruct {
	my ($self, $row) = @_;

	#my $res = { };
	#my $i = 0;
	#my @struct = @{$self->{cols}};
	#foreach my $cn (@struct) {
	#	$res->{$cn} = $row->[$i++];
	#}
	#return $res;

	my $cols = $self->{cols};
	my $n = scalar @$row - 1;
	my %res = map { $cols->[$_] => $row->[$_] } 0..$n;
	return \%res;
}

sub toColumns {
	my ($self, $row) = @_;

	my $cols = $self->{cols};
	my @values = map { $row->{$_} } @$cols;
	return @values;
}

# ========================================================================================================================

1;
