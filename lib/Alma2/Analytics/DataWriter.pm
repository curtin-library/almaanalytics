
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Analytics::DataWriter;

use strict;
use warnings;
use Data::Dumper;

use File::Basename;

use Alma2::Util::DateTime;
use Alma2::Analytics::Client;
use Alma2::Analytics::Filter;

# ========================================================================================================================
#
#		--conf_file=/data/home/alma/conf/config.yml
#		--conf_env=production.analytics
#
#		--query_path=Analytics analysis path
#		--query_columns=derived from analysis definition
#		--query_filter_file=filter file for query
#		--query_data=file containing data
#		--query_types=file containing line by line column types, '|' separated column types
#       (note filter-data-types have a one-one association across the files)
#
#		--output_source=output from query run
#		--output_format={csv|xls|xlsx|sqlite}
#		--output_file=name of output file (csv, excel, and sqlite only)
#		--output_table=name of sql table (sql save only)
#
# ========================================================================================================================

sub new {
	my ($class, $options) = @_;

	my $self = {
		conf_file       => $options->{conf_file},
		conf_env        => $options->{conf_env} . ".analytics",

		query_path      => $options->{query_path},
		query_columns   => $options->{query_columns},
		query_filters   => $options->{query_filters},
		query_types     => $options->{query_types},
		query_data      => $options->{query_data},
		query_output    => $options->{query_output},

		output_format   => $options->{output_format},	# output format is (csv|xls|xlsx|sqlite|mysql|oracle)
		output_table    => $options->{output_table},	# valid only for SQL saves
		output_types    => $options->{output_types},	# output column types (only for SQL)

		output_file     => $options->{output_file},		# output file name (sqlite)
		output_database => $options->{output_database},	# mysql
		output_host     => $options->{output_host},		# mysql
		output_port     => $options->{output_port},		# mysql
		output_sid      => $options->{output_sid},		# oracle
		output_user     => $options->{output_user},		# mysql and oracle
		output_passwd   => $options->{output_passwd},	# mysql and oracle
	};

	return bless $self, $class;
}

# ========================================================================================================================

# do any necessary conversion of parameters to Analytics query
sub conv_data {
	my ($data, $dtypes) = @_;
	my $newdata = [];
	my $n = scalar @$data;
	for (my $i = 0; $i < $n; $i++) {
		my $value = $data->[$i];
		my $type = $dtypes->[$i];
		if ( $type =~ /datetime/ ) {
			my $datetime = Alma2::Util::DateTime->new();
			$datetime->set(string => $value);
			$value = $datetime->datetimestamp('%Y-%m-%d %H:%M:%S');
		} elsif ( $type =~ /date/ ) {
			my $date = Alma2::Util::DateTime->new();
			$date->set(string => $value);
			$value = $date->datestamp('%Y-%m-%d');
		}
		$newdata->[$i] = $value;
	}
	return $newdata;
}

sub run {
	my ($self) = @_;

	# format the output file (will be stored in /tmp)
	my $ofile = $self->{query_output};
	my $file = "/tmp/$ofile";

	# open the output file
	my @colnames = @{$self->{query_columns}};
	open FILE, ">", $file;
	print FILE '"' . join('","', @colnames) . "\"\n";

	# create the Analytics client
	my $cfile = $self->{conf_file};
	my $yml = Alma2::Config::YAML->new($cfile);
	my $conf = $yml->get($self->{conf_env});
	my $dbh = Alma2::Analytics::Client->new($conf);
	my $query = $self->{query_path};
	my $coldefs;

	if ( $self->{query_filters} ) {
		my $filters = $self->{query_filters};
		my $lines = $self->{query_data};
		my $types = $self->{query_types};
		my $n = scalar @$filters;
		for (my $x = 0; $x < $n; $x++) {
			# get the next filter
			my $filter = $filters->[$x];
			my $type_set = $types->[$x];
			my $line = $lines->[$x];

			# convert data if necessary
			my $data = conv_data($line, $type_set);

			# prepare a 'statement' handle
			my $sth = $dbh->prepare($query, \@colnames) or die $dbh->errstr();

			# prepare and bind to a filter
			my $aaf = Alma2::Analytics::Filter->new();
			$aaf->initialise($filter, $type_set);
			my $xml = $aaf->compile($data);
			$sth->bind_param($xml);

			# exec the query and grab the coldefs beforehand
			$sth->execute();
			$coldefs = $sth->coldefs() if !$coldefs;

			while ( my $row = $sth->fetchrow_arrayref() ) {
				# write out .csv line
				foreach (@$row) {
					$_ =~ s/\"/\"\"/g;
				}
				print FILE '"' . join('","', @$row) . "\"\n";
			}

			# release the query handle
			$sth->finish();
		}

		# release from Analytics
		$dbh->disconnect();
	} else {
		# no filter(s), so just exec the query
		my $sth = $dbh->prepare($query, \@colnames) or die $dbh->errstr();
		$sth->execute();
		while ( my $row = $sth->fetchrow_arrayref() ) {
			# write out .csv line
			print FILE '"' . join('","', @$row) . "\"\n";
		}

		# release statement handle
		$sth->finish();

		# release from Analytics
		$dbh->disconnect();
	}

	close FILE;

	return;
}

# ========================================================================================================================

#sub map_cdefs {
#	my ($self, $list) = @_;
#	die "Unimplemented method map_cdefs in base class Alma2::Analytics::DataWriter!!";
#}
#
#sub save {
#	my ($self) = @_;
#	die "Unimplemented method save in base class Alma2::Analytics::DataWriter!!";
#}

# ========================================================================================================================

sub get_csv_dbh {
	my ($self, $infile, $cols) = @_;

	# convert @$cols values to lower case as dbd::csv works in lower case column names only
	my @columns = map { $_ =~ tr/A-Z/a-z/r; $_ } @$cols;

	# go through row-by-row of input spreadsheet as a DBI object/thing
	my ($name, $path, $suffix) = fileparse($infile, qr/\.[^.]*/);
	$path =~ s|/$||g;
	my $dbh = DBI->connect('DBI:CSV:', undef, undef, {
		f_dir => $path,
		csv_tables => {
			data => {
				col_names => [ @columns ],
				skip_first_row => 1,
				f_file => "$name$suffix",
			}
		},
	});

	return ($dbh, \@columns);
}

# ========================================================================================================================

1;
