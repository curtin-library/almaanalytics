
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Analytics::Statement;

use strict;
use warnings;
use Data::Dumper;

use XML::LibXML;
use XML::LibXML::Reader;
use XML::LibXML::XPathContext;
use POSIX;

use Alma2::REST::Client;
use Alma2::Analytics::RowStruct;
use Alma2::Analytics::Parser;
use Alma2::Analytics::Parser::XML;

# ========================================================================================================================

use constant ANALYTICS_RETRIES => 3;
#use constant DEFAULT_BATCH_SIZE => 50;
use constant DEFAULT_BATCH_SIZE => 100;
use constant MAXIMUM_BATCH_SIZE => 100;

# ========================================================================================================================

sub new {
	my ($class, $analytics, $path, $row_tmpl) = @_;

	my $rows = Alma2::Analytics::RowStruct->new($row_tmpl);

	my $this = {
		_analytics => $analytics,			# connects to the object that created this
		_client => $analytics->client,
		_path => $path,
		_row_tmpl => $rows,					# row template
		_filter => undef,					# XML filter
		_batch_size => DEFAULT_BATCH_SIZE,	# limit number of rows returned per invocation

		# NOTE: REST API returns the continuation token and row element/column definitions only once, so these need to be remembered from each call to _retrieve
		_token => undef,					# the Analytics /QueryResult/ResumptionToken continuation token, taken from the (first) meta-header returned by the API call
		_has_more => undef,					# the Analytics XML /QueryResult/IsFinished flag
		_set => undef,						# current window of result row structs
		_defs => undef,						# row element and column definitions
		_cpos => -1,						# at the start _cpos is always -1
	};

	bless $this, $class;
	return $this;
}

# ================================================================================================================

sub bind_param {
	my ($this, $filter) = @_;
	$this->{_filter} = $filter;
}

sub bind {
	my ($this, $filter) = @_;
	$this->{_filter} = $filter;
}

# ================================================================================================================

#	my $columns = $sth->coldefs();
#	my $ncols = scalar @$columns;
#	for (0..$ncols-1) {
#		$columns->[$_]->{name};
#		$columns->[$_]->{type};
#		$columns->[$_]->{pos};
#	}

sub coldefs {
	my ($self) = @_;
	return $self->{_defs}->{columns};
}

sub has_more {
	my ($self) = @_;
	return $self->{_has_more};
}

sub analytics {
	my ($self) = @_;
	return $self->{_analytics};
}

# ================================================================================================================

sub _retrieve {
	my ($this) = @_;

	# simplify inputs
	my $filter = ( defined $this->{_filter} ? $this->{_filter} : undef );
	my $analytics = $this->{_analytics};
	my $service = $this->{_client};
	my $format = $service->format();
	my $batch_size = $this->{_batch_size};
	my $path = $this->{_path};
	my $token = $this->{_token};
	my $tmpl = $this->{_row_tmpl};

	# prepare arguments for Service read
	my %arguments;
	$arguments{limit} = $batch_size;

	if (defined $token) {
		# for resuming a query, filter and path are not required, just the token.
		$arguments{token} = $token;
	} else {
		# for initiating a query, path is required.
		# filter is optional unless the report requires it.
		$arguments{path} = $path;
		if ($filter) {
			$filter =~ s/\r//g;
			$filter =~ s/\n//g;
			$arguments{filter} = $filter;
		}
	}

	# create a parser for the format
	my $parser = Alma2::Analytics::Parser::make($format);
	die "Alma2::Analytics::Statement->_retrieve() format '$format' not accepted." if !defined $parser;

	# attempt to execute the Analytics query
	my $row_element;
	for (my $retries = ANALYTICS_RETRIES; $retries > 0; --$retries) {
		# invoke Analytics API to execute and
		my $rc = $service->read($analytics->prefix(), \%arguments);
		#next if $rc;

		# check for errors
		my $result = $service->response();
		$rc = $service->error()->code();
		if ($rc ne '200') {
			my $restError = $service->api_errors();
			if ($rc eq '400') {
				if ( $restError->[0]->{code} eq '420033' ) {
					# this means "No more rows to fetch"
					$analytics->clearError();
					$this->{_cpos} = -1;
					$this->{_set} = undef;
					$this->{_token} = undef;
					#$this->{_defs} = undef;
					$this->{_has_more} = 0;
					return -1;
				}
			} else {
				my $error = Alma2::Error->new($service->client());
				$analytics->setError($error);
				return -1;
			}
		}

		# parse the response
		my $results = $parser->parse_response($result, $tmpl, $this->{_defs});
		$this->{_has_more} = $results->{has_more};
		$this->{_token} = $results->{token} if defined $results->{token};
		$this->{_set} = $results->{set};
		$this->{_defs} = $results->{defs} if defined $results->{defs};

		# decide what to do
		#if ( $results->{has_more} and !defined $results->{set} ) {
		#	# we have a resumption token but no result set.
		#	# so we retry with only the resumption token.
		#	%arguments = (
		#		limit => $batch_size,
		#		token => $results->{token},
		#	);
		#	$retries = ANALYTICS_RETRIES;
		#	next;
		#}
        #
		#$analytics->clearError();
		#$this->{_cpos} = 0;
		#return 0;

		# decide what to do
		if ( $results->{has_more} ) {
			if ( $this->{_token} ) {
				if ( $results->{set} ) {
					$analytics->clearError();
					$this->{_cpos} = 0;
					return 0;
				} else {
					# we have a resumption token but no result set, so we retry with only the resumption token.
					%arguments = (
						limit => $batch_size,
						token => $this->{_token},
					);
					$retries = ANALYTICS_RETRIES;
					next;
				}
			} else {
				# we have no resumption token when we were expecting to have one.
				if ( $results->{set} ) {
					print STDERR "Alma2::Analytics::Statement->execute - Response query is not finished and contained a result set but we do not have a token when we should.";
					$analytics->setError( -1, "Alma2::Analytics::Statement->execute - Response query is not finished and contained a result set but we do not have a token when we should." );
				} else {
					print STDERR "Alma2::Analytics::Statement->execute - Response query is not finished but did not contain a token or result set when we were expecting one.";
					$analytics->setError( -1, "Alma2::Analytics::Statement->execute - Response query is not finished but did not contain a token or result set when we were expecting one." );
				}
				$this->{_cpos} = -1;
				$this->{_set} = undef;
				$this->{_token} = undef;
				#$this->{_defs} = undef;
				$this->{_has_more} = 0;
				return -1;
			}
		} else {
			if ( $results->{set} ) {
				$analytics->clearError();
				$this->{_cpos} = 0;
				return 0;
			} else {
				$analytics->clearError();
				$this->{_cpos} = -1;
				$this->{_set} = undef;
				$this->{_token} = undef;
				#$this->{_defs} = undef;
				$this->{_has_more} = 0;
				return 0;
			}
		}
	}

	print STDERR "Alma2::Analytics::Statement->execute - giving up after ".ANALYTICS_RETRIES." tries.\n";
	$analytics->setError( -1, "Alma2::Analytics::Statement->execute - giving up after ".ANALYTICS_RETRIES." tries" );
	$this->{_cpos} = -1;
	return -1;
}

sub execute {
	my ($this) = @_;
	# do this cos of a strange behaviour - $this->{_token} is populated when it shouldn't be.
	$this->{_token} = undef;
	my $rc = $this->_retrieve();
	return ($rc != 0 ? 0 : 1);
}

# ================================================================================================================

sub _fetchrow {
	my ($this) = @_;

	# if cpos < 0 then we have not called execute yet, so return undef
	my $cpos = $this->{_cpos};
	return undef if ( $cpos < 0 );

	# test if current post lies outside of window...
	my $batch_size = $this->{_batch_size};
	if ($cpos >= $batch_size) {
		# ...it does, so grab some more rows from Analytics
		my $rc = $this->_retrieve();
		return undef if ($rc != 0);
	}

	my $row = $this->{_set}->[$this->{_cpos}++];
	return $row;
}

sub fetchrow_hashref () {
	my ($this) = @_;
	my $row = $this->_fetchrow();
	return $row;
}

sub fetchrow_arrayref () {
	my ($this) = @_;
	my $row = $this->_fetchrow();
	return undef if !defined $row;
	my $tmpl = $this->{_row_tmpl};
	my @columns = $tmpl->toColumns($row);
	return \@columns;
}

# ========================================================================================================================

sub finish () {
	my ($this) = @_;
	$this->{_analytics} = undef;
	$this->{_client} = undef;
	$this->{_row_tmpl} = undef;
	$this->{_filter} = undef;
	$this->{_token} = undef;
	$this->{_has_more} = undef;
	$this->{_set} = undef;
}

# ========================================================================================================================

1;
