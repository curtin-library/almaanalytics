
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

# ========================================================================================================================
# operators supported:
# and      : and
# or       : or
# in       : in
# !in      : notIn
# like     : like
# !like    : notLike
# null     : null
# !null    : notNull
# between  : between
# eq       : equal
# ne       : notEqual
# in       : in
# lt       : lessThan
# le       : lessOrEqual
# gt       : greater
# ge       : greaterOrEqual
#
# operators not supported:
# ranked first
# ranked last
# contains all
# contains any
# does not contain
# begins with
# ends with
# ========================================================================================================================

package Alma2::Analytics::Filter;

use strict;
use warnings;
use Data::Dumper;

use HOP::Lexer qw(make_lexer);

# ========================================================================================================================

sub new {
	my ($class) = @_;

	my $preamble = 'xmlns:saw="com.siebel.analytics.web/report/v1.1" ' .
	               'xmlns:sawx="com.siebel.analytics.web/expression/v1.1" ' .
	               'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' .
	               'xmlns:xsd="http://www.w3.org/2001/XMLSchema"';

	my $self = {
		siebold_preamble => $preamble,
		filter => undef,
		types => undef,
		vlist => undef,
		vindex => -1,
		root => undef,
		curr => undef,
		params => undef,
	};
	bless $self, $class;

	return $self;
}

# ========================================================================================================================

sub mknode {
	my ($type) = @_;

	my $node = {
		type => $type,
		parent => undef,
		next => undef,
		child => undef,
	};

	return $node;
}

sub mknode_operator {
	my ($self, %args) = @_;

	my $node = mknode('OPER');
	$node->{op_name} = $args{op_name};

	return $node;
}

sub mknode_field {
	my ($self, %args) = @_;

	my $node = mknode('FIELD');
	$node->{field_name} = $args{field_name};

	return $node;
}

sub mknode_array {
	my ($self, %args) = @_;

	my $node = mknode('LIST');
	$node->{list} = [];

	return $node;
}

sub mknode_param {
	my ($self, %args) = @_;

	my $node = mknode('PARAM');
	$node->{value_type} = $args{type};

	return $node;
}

sub scan_end {
	my ($node) = @_;
	while ($node->{next}) {
		$node = $node->{next};
	}
	return $node;
}

sub pre_compile {
	my ($self) = @_;

	my $text = $self->{filter};
	my @text = ($text);
	my $lexer = make_lexer(
		sub { shift @text; },
		[ 'FIELD' , qr/(?:"[\w\d\s]*"(?:\."[\w\d\s]*"){1,2})/,                                    sub { [ shift, shift ] }  ],
		[ 'OPER'  , qr/(?i:\!in|\!like|\!null|and|or|in|like|null|between|lt|le|eq|ne|ge|gt)/,    sub { [ shift, shift ] }  ],
		[ 'PARAM' , qr/\?/,                                                                       sub { [ shift,  0 ] }     ],
		[ 'PAREN' , qr/\(/,                                                                       sub { [ shift,  1 ] }     ],
		[ 'PAREN' , qr/\)/,                                                                       sub { [ shift, -1 ] }     ],
		[ 'LIST'  , qr/\[/,                                                                       sub { [ shift,  1 ] }     ],
		[ 'LIST'  , qr/\]/,                                                                       sub { [ shift, -1 ] }     ],
		[ 'SPACE' , qr/\s*/,                                                                                                ],
		[ 'COMMA' , qr/,/,                                                                                                  ]
	);

	while ( defined ( my $token = $lexer->() ) ) {
		my ( $label, $value ) = @$token;

		my $curr = $self->{curr};

		if ($label eq 'OPER') {
			my $node = $self->mknode_operator(op_name => $value);

			if (defined $self->{root}) {
				$node->{parent} = $curr;
				if (defined $curr->{child}) {
					my $end = scan_end($curr->{child});
					$end->{next} = $node;
				} else {
					$curr->{child} = $node;
				}
				$self->{curr} = $node;
			} else {
				$self->{root} = $node;
				$self->{curr} = $node;
			}
		} elsif ( $label eq 'PAREN' ) {
			if ( $value < 0 ) {
				$self->{curr} = $curr->{parent} if ($curr->{parent});
			}
		} elsif ( $label eq 'LIST' ) {
			if ( $value > 0 ) {
				# allocate an array node
				my $node = $self->mknode_array();
				$node->{parent} = $curr;

				# set node to array
				if (defined $curr->{child}) {
					my $end = scan_end($curr->{child});
					$end->{next} = $node;
				} else {
					$curr->{child} = $node;
				}

				# move to current
				$self->{curr} = $node;
			} else {
				# move to parent
				$self->{curr} = $curr->{parent};
			}
		} elsif ( $label eq 'FIELD' ) {
			# allocate a field node with name
			my $node = $self->mknode_field(field_name => $value);
			$node->{parent} = $curr;

			# add node to $self->{curr}
			if (defined $curr->{child}) {
				my $end = scan_end($curr->{child});
				$end->{next} = $node;
			} else {
				$curr->{child} = $node;
			}
		} elsif ( $label eq 'PARAM' ) {
			# get next positional parameter's value type
			$self->{vindex}++;
			my $type = $self->{vlist}->[$self->{vindex}];

			# allocate value node
			my $node = $self->mknode_param(type => $type);
			$node->{parent} = $curr;

			if ($curr->{type} eq 'LIST') {
				# append to array
				my $list = $curr->{list};
				push @$list, $node;
			} elsif ($curr->{type} eq 'OPER') {
				# append as child to current
				if (defined $curr->{child}) {
					my $end = scan_end($curr->{child});
					$end->{next} = $node;
				} else {
					$curr->{child} = $node;
				}
			}
		} else {
			next;
		}
	}

	$self->{vindex} = 0;
}

sub initialise {
	my ($self, $filter, $types) = @_;

	if ($self->{root}) {
		$self->{root} = undef;
		$self->{curr} = undef;
		$self->{last} = undef;
		$self->{filter} = undef;
		$self->{vlist} = undef;
	}

	$self->{vlist} = $types;
	$self->{filter} = $filter;
	$self->pre_compile();
}

# ========================================================================================================================

sub xlat_opname {
	my ($op) = @_;
	return 'less'           if $op =~ /^lt$/i;
	return 'lessOrEqual'    if $op =~ /^le$/i;
	return 'equal'          if $op =~ /^eq$/i;
	return 'notEqual'       if $op =~ /^ne$/i;
	return 'greater'        if $op =~ /^gt$/i;
	return 'greaterOrEqual' if $op =~ /^ge$/i;
	return 'notIn'          if $op =~ /^\!in$/i;
	return 'notLike'        if $op =~ /^\!like$/i;
	return 'notNull'        if $op =~ /^\!null$/i;
	return $op              if $op =~ /^(?:and|or|in|like|null|between)$/i;
	return $op;
}

sub do_compile_oper {
	my ($self, $node) = @_;

	my $op = xlat_opname($node->{op_name});

	my $preamble = '';
	$preamble = ' '.$self->{siebold_preamble} if ($node == $self->{root});

	my $xml;
	if ($op =~ /(?:and|or)/) {
		$xml = sprintf('<sawx:expr xsi:type="sawx:logical" op="%s"%s>' . "\n", $op, $preamble);
	} elsif ($op =~ /(?:notIn|notLike|in|like)/) {
		$xml = sprintf('<sawx:expr xsi:type="sawx:list" op="%s"%s>' . "\n", $op, $preamble);
	} else {
		$xml = sprintf('<sawx:expr xsi:type="sawx:comparison" op="%s"%s>' . "\n", $op, $preamble);
	}

	my $child = $node->{child};
	while ($child) {
		$xml .= $self->do_compile($child);
		$child = $child->{next};
	}
	$xml .= "</sawx:expr>\n";

	return $xml;
}

sub do_compile_field {
	my ($self, $node) = @_;
	my $field_name = $node->{field_name};
	my $xml = sprintf('<sawx:expr xsi:type="sawx:sqlExpression">%s</sawx:expr>' . "\n", $field_name);
	return $xml;
}

sub do_compile_list {
	my ($self, $node) = @_;

	my $xml;
	my $list = $node->{list};
	foreach my $list_node (@$list) {
		$xml .= $self->do_compile($list_node);
	}
	return $xml;
}

# no data type checking. assumed valid. but take note, only { date, dateTime, string } are really supported by Oracle BI Analytics.
sub do_compile_param {
	my ($self, $node) = @_;

	my $params = $self->{params};
	my $type = $self->{vlist}->[$self->{vindex}];
	my $value = $params->[$self->{vindex}];
	$self->{vindex}++;

	my $xml = sprintf('<sawx:expr xsi:type="xsd:%s">%s</sawx:expr>' . "\n", $type, $value);
	return $xml;
}

sub do_compile {
	my ($self, $node) = @_;

	my $xml;
	my $ntype = $node->{type};
	if ($ntype eq 'OPER') {
		$xml .= $self->do_compile_oper($node);
	} elsif ($ntype eq 'FIELD') {
		$xml .= $self->do_compile_field($node);
	} elsif ($ntype eq 'LIST') {
		$xml .= $self->do_compile_list($node);
	} elsif ($ntype eq 'PARAM') {
		$xml .= $self->do_compile_param($node);
	}
	return $xml;
}

sub compile {
	my ($self, $params) = @_;
	$self->{params} = $params;
	my $xml = $self->do_compile($self->{root});
	$self->{vindex} = 0;
	return $xml;
}

# ========================================================================================================================

1;
