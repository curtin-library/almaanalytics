
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Analytics::DataWriter::Excel;

use strict;
use warnings;
use Data::Dumper;

use base "Alma2::Analytics::DataWriter";

# ========================================================================================================================
#
# We use CPAN modules Spreadsheet::WriteExcel (for xls) and Excel::Writer::XLSX (for xlsx)
# because both classes offer the same methods for writing to the spreadsheet. This makes
# it easier to use just one module for both spreadsheet types.
#
# ========================================================================================================================

use  Spreadsheet::WriteExcel;
use Excel::Writer::XLSX;

# ========================================================================================================================

sub new {
	my ($class, $options) = @_;
	my $self = Alma2::Analytics::DataWriter->new($options);
	bless $self, $class;
	return $self;
}

# ========================================================================================================================

sub map_cdefs {
	# $list is an array of result set types returned by the the Analytics query
	# we need to match and turn into Excel types
	my ($self, $list) = @_;

	# Oracle Analytics to sqlite data type mapping table
	my $typemap = {
		'text' => 'text',
		'varchar' => 'text',
		'ntext' => 'text',
		'date' => 'text',
		'datetime' => 'text',
		'timestamp' => 'text',
		'id' => 'text',
		'double' => 'real',
		'int' => 'integer',
		'integer' => 'integer',
		'shortinteger' => 'integer',
		'longinteger' => 'integer',
		'decimal' => 'integer',
		'shortdecimal' => 'integer',
		'number' => 'integer',
		'numeric' => 'integer',
	};

	# integer -> no need format
	# varchar -> text
	# date -> datetime format yyyy-mm-dd
	# timestamp -> datetime format yyyy-mm-dd hh:mm:ss
	my @qrtypes;
	foreach (@$list) {
		my $s = $_;
		$s =~ tr/A-Z/a-z/r;
		die "Alma2::Analytics::DataWriter::Excel.map_cdefs() encountered invalid type ".$_ if (!defined $typemap->{$s});
		push @qrtypes, $typemap->{$s};
	}

	return \@qrtypes;
}

# ========================================================================================================================

sub save {
	my ($self) = @_;

	my @cnames = @{$self->{query_columns}};
	@cnames = map { $_ =~ tr/A-Z/a-z/; $_ } @cnames;
	my $infile = "/tmp/".$self->{query_output};
	my $outfile = $self->{output_file};
	my $restypes = $self->{output_types};

	# create output file
	my $workbook;
	if ( $self->{output_format} =~ /xlsx/ ) {
		$workbook = Excel::Writer::XLSX->new($outfile);
	} else {
		$workbook = Spreadsheet::WriteExcel->new($outfile);
	}

	# create excel data formats
	my $dateformat = $workbook->add_format(num_format => 'yyyy-mm-dd', align => 'left');
	my $datetimeformat = $workbook->add_format(num_format => 'yyyy-mm-dd h:mm:ss', align => 'left');
	my $textformat = $workbook->add_format(align => 'left');
	my $numberformat = $workbook->add_format(num_format => '@', align => 'right');

	# write out column headers
	my $currRow = 0;        # 0 is Row 1
	my $currCol = 0;        # 0 is Column A, 1 is Column B, ...
	my $worksheet = $workbook->add_worksheet();
	my $cols = $self->{query_columns};
	foreach (@$cols) {
		$worksheet->write_string($currRow, $currCol++, $_, undef);
	}
	$currRow++;
	$currCol = 0;

	# get dbh and lower-case $cols
	my ($dbh, $columns) = $self->get_csv_dbh($infile, \@cnames);

	# read csv data from .csv infile and write to xls spreadsheet
	my $sth = $dbh->prepare('select * from data');
	$sth->execute;
	$sth->fetchrow_arrayref;

	my $n = scalar @cnames;
	while ( my $row = $sth->fetchrow_hashref ) {
		$currCol = 0;
		for (my $i = 0; $i < $n; $i++) {
			my $value = $row->{$cnames[$i]};
			my $type = $restypes->[$i];

			if ($type =~ /datetime/i) {
				$value =~ s/T/ /g;
				$worksheet->write_date_time($currRow, $currCol++, $value, $dateformat);
			} elsif ($type =~ /date/i) {
				$value =~ s/T/ /g;
				$worksheet->write_date_time($currRow, $currCol++, $value, $datetimeformat);
			} elsif ($type =~ /bool/i) {
				$value = ($value ? 1 : 0);
				$worksheet->write_number($currRow, $currCol++, $value, $numberformat);
			} elsif ($type =~ /integer/i) {
				$worksheet->write_number($currRow, $currCol++, $value, $numberformat);
			} else {
				$worksheet->write_string($currRow, $currCol++, $value, $textformat);
			}
		}
		$currRow++;

		if ($currRow == 1045876) {
			warn "Maximum number of Excel rows will be exceeded. Data truncated.";
			last;
		}
	}

	$sth->finish();
	$dbh->disconnect();

	$workbook->close();
}

# ========================================================================================================================

1;
