
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Analytics::DataWriter::SQL;

use strict;
use warnings;
use Data::Dumper;

use base "Alma2::Analytics::DataWriter";

use DBI;

# ========================================================================================================================

sub new {
	my ($class, $options) = @_;
	my $self = Alma2::Analytics::DataWriter->new($options);
	bless $self, $class;
	return $self;
}

# ========================================================================================================================

sub suggest {
	my ($self) = @_;
	die "Alma2::Analytics::DataWriter::SQL.suggest() not implemented.";
}

sub map_cdefs {
	my ($self, $list) = @_;
	die "Alma2::Analytics::DataWriter::SQL.map_cdef() not implemented.";
}

sub conv_data {
	my ($self) = @_;
	die "Alma2::Analytics::DataWriter::SQL.conv_data() not implemented.";
}

# ========================================================================================================================

#	Index      Column Meta-info
#	0          ?
#	1          ?
#	2          table name
#	3          column name
#	4          ?
#	5          column value type
#	6          ?
#	7          ?
#	8          ?
#	9          ?
#	10         ?
#	11         ?
#	12         ?
#	13         ?
#	14         ?
#	15         ?
#	16         column number
#	17         ? YES|NO

sub match_columns {
	my ($self, $dbcols, $cnames) = @_;

	my $n = scalar @$dbcols;
	for (my $i = 0; $i < $n; $i++) {
		my $set = $dbcols->[$i];
		my $colname = $set->[3];
		my $cname = $cnames->[$i];

		die "Alma2::Analytics::DataWriter::SQL.match_columns() failed because database column name[$i] (".$colname.") does not match configured column name (".$cname.")"
			if $colname ne $cname;
	}

	return;
}

sub match_ctypes {
	my ($self, $dbcols, $ctypes) = @_;

	my $n = scalar @$dbcols;
	for (my $i = 0; $i < $n; $i++) {
		my $set = $dbcols->[$i];
		my $coltype = $set->[5];
		my $ctype = $ctypes->[$i];

		die "Alma2::Analytics::DataWriter::SQL.match_columns() failed because database column name[$i] (".$coltype.") does not match configured column name (".$ctype.")"
			if $coltype ne $ctype;
	}

	return;
}

sub verify_table {
	my ($self, $dbh, $table, $cnames, $ctypes) = @_;

	die "SQLite.verify_table failed because input column names and types do not match one-to-one."
		if scalar @$cnames != scalar @$ctypes;

	# check sanity of and act on parameters
	my $tinfo = $dbh->table_info(undef, undef, $table);
	if ( ! scalar @{$tinfo->fetchall_arrayref()} ) {
		# no info returned implies $table does not exist
		# suggest SQL statmenet to create with every column as text
		my $stmt = $self->suggest();

		my $message = "Alma2::Analytics::DataWriter::SQL.verify_table() failed because $table does not exist.\nPlease create the table first.\n" .
			"Suggested query:\n" .
			$stmt . "\n";

		die $message;
	}

	# ==========
	# there is info returned so we have a table, but check first that schema matches input
	# ==========
	# column count - number of columns in database table must match what is configured
	my $cref = $dbh->column_info(undef, undef, $table, undef)->fetchall_arrayref();
	my @dbcols = @$cref;
	shift @dbcols if $dbcols[0]->[3] =~ /id/i;	# we shift if there is an ID column

	die "Alma2::Analytics::DataWriter::SQL.verify_table() failed because database schema does not match configured number of columns."
		if scalar @dbcols != scalar @$cnames;

	die "Alma2::Analytics::DataWriter::SQL.verify_table() failed because database schema does not match configured number of types."
		if scalar @dbcols != scalar @$ctypes;

	# column names and types in database table must match what is configured
	$self->match_columns(\@dbcols, $cnames);
	$self->match_ctypes(\@dbcols, $ctypes);
}

sub save {
	my ($self) = @_;

	my $list    = $self->{output_types};
	my $infile  = "/tmp/".$self->{query_output};
	my $table   = $self->{output_table};
	my $cnames  = $self->{query_columns};
	my $ctypes  = $self->{output_types};

	# get dbh
	my $sqldbh = $self->get_connection();

	# verify the table is consistent with configuration
	$self->verify_table($sqldbh, $table, $cnames, $ctypes);

	# prepare query handle
	my $dbcols = join(",", @$cnames);
	my $ncols = scalar @$cnames;
	my $values = '?,' x ($ncols - 1);
	$values .= '?';

	my $qs = qq/insert into $table ($dbcols) values ($values)/;
	$qs =~ tr/a-z/A-Z/;
	my $sqlsh = $sqldbh->prepare($qs);

	# get dbh (dbd::csv) and lower-case $cols (DBD::CSV operates only in lower case column names)
	my ($csvdbh, $columns) = $self->get_csv_dbh($infile, $cnames);
	my $csvsh = $csvdbh->prepare('select * from data');
	$csvsh->execute() or die $csvsh->errstr();
	$csvsh->fetchrow_arrayref();	# skips column titles line (the first) in .csv file

	# for each row from .csv file write into SQLite table
	my $i = 0;
	while ( my $inrow = $csvsh->fetchrow_arrayref() ) {
		my @values = map { $self->conv_data($inrow->[$_], $ctypes->[$_]) } 0..$ncols-1;
		$sqlsh->bind_param( $_ + 1, $values[$_] ) for (0..$ncols-1);
		my $ok = $sqlsh->execute();
		if (!$ok) {
			my $msg = "runrep.pl save_db() failed to insert record #$i (".$DBI::errstr.")";
			die $msg;
		}
		$i++;
	}

	$csvsh->finish();
	$sqlsh->finish();

	$sqldbh->disconnect();
	$csvdbh->disconnect();

	return;
}

# ========================================================================================================================

1;
