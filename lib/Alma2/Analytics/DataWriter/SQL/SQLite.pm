
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Analytics::DataWriter::SQL::SQLite;

#
# Sample SQLite 3 instructions (just for documentation)
#
#	$ sqlite3 myreport1.db
#	sqlite> CREATE TABLE MYREPORT1 (
#	...> ID INTEGER PRIMARY KEY AUTOINCREMENT,
#	...> ISBN TEXT NOT NULL,
#	...> MMS_ID TEXT NOT NULL,
#	...> MODIFICATION_DATE TEXT,
#	...> TITLE NOT NULL,
#	...> CURRENT_DATE TEXT
#	);
#	sqlite> .schema myreport1
#	sqlite> .quit
#

use strict;
use warnings;
use Data::Dumper;

use base "Alma2::Analytics::DataWriter::SQL";

use DBI;

# ========================================================================================================================

sub new {
	my ($class, $options) = @_;
	my $self = Alma2::Analytics::DataWriter::SQL->new($options);
	bless $self, $class;
	return $self;
}

# ========================================================================================================================

sub suggest {
	my ($self) = @_;
	my $cnames = $self->{query_columns};
	my $ctypes = $self->{output_types};
	my $table = $self->{output_table};
	my $stmt = "CREATE TABLE $table (\n\tID INTEGER PRIMARY KEY AUTOINCREMENT";
	my $x = 0;
	for (@$cnames) {
		my $colname = $_;
		$colname =~ tr/a-z/A-Z/;

		my $type = $ctypes->[$x++];
		$type =~ tr/a-z/A-Z/;

		$stmt .= ",\n\t$colname $type NOT NULL";
	}
    $stmt .= "\n)";
}

# ========================================================================================================================

sub map_cdefs {
	my ($self, $list) = @_;

	# Oracle Analytics to sqlite data type mapping table
	my $typemap = {
		'string' => 'text',
		'text' => 'text',
		'varchar' => 'text',
		'ntext' => 'text',
		'date' => 'text',
		'datetime' => 'text',
		'timestamp' => 'text',
		'id' => 'text',
		'double' => 'real',
		'int' => 'integer',
		'integer' => 'integer',
		'shortinteger' => 'integer',
		'longinteger' => 'integer',
		'decimal' => 'integer',
		'shortdecimal' => 'integer',
		'number' => 'integer',
		'numeric' => 'integer',
	};

	my $n = scalar @$list;
	my @cdefs;
	foreach my $type (@$list) {
		if ($type =~ /(longinteger|decimal|number|numeric)/) {
			warn "Alma2::Analytics::DataWriter::SQLite.map_cdefs() encountered $type. possible overflow in storage.";
		}
		$type = $typemap->{$type};
		die "Alma2::Analytics::DataWriter::SQLite.map_cdefs() encountered unknown data type ".$type."." if (!defined $type);
		push @cdefs, $type;
	}

	return \@cdefs;
}

# ========================================================================================================================

sub conv_data {
	my ($self, $value, $type) = @_;

	if ($type eq 'boolean') {
		if ( $value =~ /(true|yes|on|t|y|1)/i ) {
			$value = 'true';
		} elsif ( $value =~ /(false|no|off|f|n|0)/i ) {
			$value = 'false';
		} else {
			$value = 'false'
		}
	}

	return $value;
}

# ========================================================================================================================

sub get_connection {
	my ($self) = @_;
	my $outfile = $self->{output_file};
	my $sqldbh = DBI->connect("DBI:SQLite:dbname=$outfile", '', '') or die $DBI::errstr;
	return $sqldbh;
}

# ========================================================================================================================

1;
