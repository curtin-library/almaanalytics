
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Mail::Unix::Writer;

use strict;
use warnings;
use Data::Dumper;
use MIME::Entity;

# ========================================================================================================================

use Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw( send_mime send_text BATCH_SIZE );
our %EXPORT_TAGS = ( all => [qw( send_mime send_text BATCH_SIZE )], );

# ========================================================================================================================

use constant BATCH_SIZE => 20;

# ========================================================================================================================

sub do_send {
	my ($mailer) = @_;

	# Send it
	open MAIL, "| /usr/lib/sendmail -t -oi -oem" or return -1;
	$mailer->print(\*MAIL);
	close MAIL;
	return 0;
}

# ========================================================================================================================

#
# Inputs: From
#         To
#         Subject
#         Body
#         Data
#

sub send_text {
	my ($args) = @_;

	# construct MIME mailer object header
	my $mailer = MIME::Entity->build(
		Type    => 'text/plain; charset=UTF-8',
		From    => $args->{From},
		To      => $args->{To},
		Subject => $args->{Subject},
		Data    => $args->{Message});
	$mailer->head()->add('Cc', $args->{Cc}) if $args->{Cc};

	# set the body
	#$mailer->attach(Data => $args->{Message});

	my $rc = do_send($mailer);
	return $rc;
}

#
# Inputs: From
#         To
#         Subject
#         Body
#         Data
#
#	my $tmpfile = '/tmp/body.txt';
#	my $body = { Path => $tmpfile, };
#
#	my $filenames = [qw( abc.html def.jpg ghi.css jkl.js mno.xml pqrstuvw.json xyz.xml )];
#	my $attachments = [];
#	my $path = '/data/files';
#	foreach my $filename (@$filenames) {
#		my $fullpath = "$path/$filename";
#		push @$attachments, { Type => "application/octet-stream", Path => $fullpath, };
#	}
#
#	my $rc = Mail::Unix::Writer::send_mime({
#		From    => 'sysadm@server.com',
#		To      => 'recipient@host.com',
#		Subject => 'Job completed',
#		Body    => $body,
#		Data    => $attachments,
#	});
#

sub send_mime {
	my ($args) = @_;

	# construct MIME mailer object header
	my $mailer = MIME::Entity->build(Type => 'multipart/mixed');
	my $head = $mailer->head();
	$head->add("From", $args->{From});
	$head->add("To", $args->{To});
	$head->add("Subject", $args->{Subject});
	$head->add('Cc', $args->{Cc}) if $args->{Cc};

	# set the mail body (cannot use Data as that is for single-entity only)
	#$mailer->attach(Type => 'text/plain', Path => $args->{Message}) if $args->{Message};
	$mailer->attach(Type => 'text/plain', Data => $args->{Message}) if $args->{Message};

	# add the data files - assumed to be an array
	my $data = $args->{Data};
	if ($data) {
		foreach my $att (@$data) {
			$mailer->attach(Disposition => 'attachment', Encoding => 'base64', Type => $att->{Type}, Path => $att->{Path});
		}
	}

	my $rc = do_send($mailer);
	return $rc;
}

# ========================================================================================================================

1;
