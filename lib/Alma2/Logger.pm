
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

package Alma2::Logger;

use strict;
use warnings;
use Data::Dumper;

use IO::Handle;
use IO::File;

use Alma2::Defaults;
use Alma2::Util::DateTime;

# ========================================================================================================================

use constant NoLog => 0;
use constant Fatal => 1;
use constant Error => 2;
use constant Warn  => 4;
use constant Debug => 8;
use constant Info  => 16;

# ========================================================================================================================

#sub new {
#	my ($class, $datetime, $logname) = @_;
#
#	my $self = {
#		level => '',
#		filename => '',
#		handle => '',
#	};
#	bless $self, $class;
#
#	$self->{level} = Warn;
#
#	#my $dts = $datetime->{date} . "." . $datetime->{time};
#	my $dts = $datetime->{date};
#	$self->{filename} = Defaults::LOG_ROOT . "/" . $dts . "." . $logname;
#
#	open my $h, ">>", $self->{filename};
#	$self->{handle} = $h;
#
#	$self->log(Info, "================================================================================", __FILE__, __LINE__);
#	$self->log(Info, "Logfile opened.", __FILE__, __LINE__);
#
#	return $self;
#}

sub new {
	# $options{date|time|logname}
	my ($class, %options) = @_;

	my $self = {
		level => '',
		filename => '',
		handle => '',
	};
	bless $self, $class;

	$self->{level} = Warn;

	#my $dts = $options{date} . "." . $options{time};
	my $dts = $options{date};
	$self->{filename} = Alma2::Defaults::LOG_ROOT . "/" . $dts . "." . $options{logname};

	my $h;
#	if (-e $self->{filename}) {
#		chmod 0664, $self->{filename};
#		open $h, ">>", $self->{filename};
#	} else {
#		open $h, ">>", $self->{filename};
#		close $h;
#		chmod 0664 $self->{filename};
#		open $h, ">>", $self->{filename};
#	}
	open $h, ">>", $self->{filename};
	$self->{handle} = $h;
	$h->autoflush(1);

	$self->log(Info, "================================================================================", __FILE__, __LINE__);
	$self->log(Info, "Logfile opened.", __FILE__, __LINE__);

	return $self;
}

# ========================================================================================================================

sub useall_log {
	my ($logname, $logLevel, $date) = @_;

	# get today's date and time
	my $dt = Alma2::Util::DateTime->new();
	if (!defined $date) {
		$date = $dt->datestamp("%Y%m%d");
	}

	# create the logger and set the logging
	my $log = Alma2::Logger->new(date => $date, logname => $logname);
	$log->add_level($logLevel);

	# connect stdout and stderr to logger handle, so we catch the dies that will occur.
	$log->connect(*STDOUT);
	$log->connect(*STDERR);

	# return the log
	return $log;
}

# ========================================================================================================================

sub translate_level {
	my ($level) = @_;
	return 'FATAL' if ($level == Fatal);
	return 'ERROR' if ($level == Error);
	return 'DEBUG' if ($level == Debug);
	return 'WARN'  if ($level == Warn);
	return 'INFO'  if ($level == Info);
	return '';
}

sub filename {
	my ($self) = @_;
	return $self->{filename};
}

sub connect {
	my ($self, $ioh) = @_;
	$ioh->fdopen($self->{handle}, 'w');
}

# ========================================================================================================================

#sub log {
#	my ($self, $level, $message, $module, $line) = @_;
#	my $dt = Alma2::Util::DateTime->new();
#	my $dts = $dt->datestamp() . ' ' . $dt->timestamp();
#	my $s = sprintf("[%s][%s][%s][%s][%s]\n", $dts, translate_level($level), $module, $line, $message);
#	my $fh = $self->{handle};
#	print $fh $s;
#	#$self->fhsync();
#}

sub log {

	my ($self, $level, $message, $module, $line) = @_;

	my $dt = Alma2::Util::DateTime->new();
	my $dts = $dt->datestamp() . ' ' . $dt->timestamp();
	my $fh = $self->{handle};
	my $tlevel = translate_level($level);

	if ( !ref($message) ) {
		my @lines = split(/\n/, $message);
		foreach (@lines) {
			print $fh sprintf("[%s][%s][%s][%s][%s]\n", $dts, $tlevel, $module, $line, $_);
		}
	} else {
		if ( ref($message) eq 'ARRAY' ) {
			foreach (@$message) {
				print $fh sprintf("[%s][%s][%s][%s][%s]\n", $dts, $tlevel, $module, $line, $_);
			}
		} elsif ( ref($message) eq 'HASH' ) {
			my $s = Dumper($message);
			my @lines = split(/\n/, $s);
			foreach (@lines) {
				print $fh sprintf("[%s][%s][%s][%s][%s]\n", $dts, $tlevel, $module, $line, $_);
			}
		}
	}

	return;

}

sub fatal {
	my ($self, $message, $module, $line) = @_;
	return if (($self->{level} & Fatal) != Fatal);
	$self->log(Fatal, $message, $module, $line);
	die $message;
}

sub error {
	my ($self, $message, $module, $line) = @_;
	return if (($self->{level} & Error) != Error);
	$self->log(Error, $message, $module, $line);
}

sub debug {
	my ($self, $message, $module, $line) = @_;
	return if (($self->{level} & Debug) != Debug);
	$self->log(Debug, $message, $module, $line);
}

sub warn {
	my ($self, $message, $module, $line) = @_;
	return if (($self->{level} & Warn) != Warn);
	$self->log(Warn, $message, $module, $line);
}

sub info {
	my ($self, $message, $module, $line) = @_;
	return if (($self->{level} & Info) != Info);
	$self->log(Info, $message, $module, $line);
}

# ========================================================================================================================

sub clear_levels {
	my ($self) = @_;
	$self->{level} = NoLog;
}

sub add_level {
	my ($self, $level) = @_;
	$self->{level} |= $level;
}

sub flush {
	my ($self) = @_;
	return if !defined $self->{handle};
	$self->log(Info, "Closing logfile.", __FILE__, __LINE__);
	close $self->{handle};
}

sub fhsync {
	my ($self) = @_;
	my $fh = $self->{handle};
        my $ofh = select($fh);
        $| = 1;
        print $fh '';
        $| = 0;
        select($ofh);
}

# ========================================================================================================================

1;
