#!/usr/bin/env perl

# site.pl: cd ./alma2.0.0.1/Alma2-0.01/custom
#          edit site.yml to set up install base directory
#          ./site.pl

use strict;
use warnings;
use Data::Dumper;

use YAML;
use Cwd;
use File::Copy qw(cp mv);
use File::Copy::Recursive qw(dircopy);
use File::Slurp qw(read_file);
use File::Path qw(make_path);
use File::Basename qw(fileparse dirname basename);

# ========================================================================================================================

# CONFIGURE_VARS:

my $ymlname = "site.yml";
die "$ymlname not found. Did you switch to the site directory before running site.pl?" if !-e $ymlname;
die "$ymlname is not a file. Are you in the correct site directory?" if !-f $ymlname;

my $s = read_file("./$ymlname");
my $cfg = YAML::Load($s);
my $site = $cfg->{site};
my $alma = $site->{alma};
my $email = $site->{email};
my $oracle = $site->{oracle};

my %configure_vars = (
	'site.alma.home'     => $alma->{home},
	'site.alma.archives' => $alma->{archives},
	'site.alma.data'     => $alma->{data},
	'site.alma.logs'     => $alma->{logs},
	'site.alma.conf'     => $alma->{conf},
	'site.email.system'  => $email->{system},
	'site.oracle.home'   => $oracle->{home},
	'site.oracle.path'   => $oracle->{home},
);

# ========================================================================================================================

fix_file();

exit;

# ========================================================================================================================

sub fix_file {

	my $file = "../lib/Alma2/Defaults.pm";
	my $tmpl = "Defaults.pm.tmpl";
	my $filename = "Defaults.pm";

	print "Fixing $file\n";
	my $s0 = "/tmp/$filename.s0";
	cp($tmpl, $s0);
	my $s1 = "/tmp/$filename.s1";

	foreach my $key (keys %configure_vars) {
		my $value = $configure_vars{$key};
		my $cmd = "cat $s0 | sed 's|\\@" . $key . "\\@|$value|g' > $s1";
		qx($cmd);
		mv($s1, $s0);
	}

	mv($s0, $file);
	unlink $s1 if (-e $s1);

}
#
# ========================================================================================================================
