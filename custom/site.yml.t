---
site:
  alma:
    home: "root directory where Alma data will be stored"
    ver: "for now this should be Alma2"
    log: "where you want your log files to be written to by the Alma2::Logger class"
    conf: "the full path to the directory where you want to store your configuration files"
  email:
    system: "this should be the email of the system administrator"
  oracle:
    home: "valid home directory for Oracle DBI module (not part of this distribution). eg. /opt/local/oracle/11.1/product"
    path: "path to Oracle executables directory. eg. /opt/local/oracle/11.1/product"
