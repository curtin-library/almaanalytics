#!/data/alma/perl5/perlbrew/perls/perl-5.20.1/bin/perl

# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use Data::Dumper;

use DBI;

#create a table
#call table_info
#call column_info

my $dbfile = "test.db";
my $dbh = DBI->connect("DBI:SQLite:dbname=$dbfile", '', '');

my $qs = qq/CREATE TABLE TEST (
	ID INT PRIMARY KEY NOT NULL,
	TEXT_VALUE TEXT NOT NULL,
	INTEGER_VALUE INTEGER NOT NULL
)/;
my $sth = $dbh->do($qs);

my $tinfo = $dbh->table_info(undef, undef, 'TEST');
my $aref = $tinfo->fetchall_arrayref();

my $cinfo = $dbh->column_info(undef, undef, 'TEST', undef);
my $cref = $cinfo->fetchall_arrayref();

exit;
