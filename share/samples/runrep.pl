#!/data/alma/perl5/perlbrew/perls/perl-5.20.1/bin/perl

# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use Data::Dumper;

use File::Slurp qw(slurp);
use File::Basename;

use Alma2::Config;
use Alma2::Analytics::DataWriter;
use Alma2::Analytics::DataWriter::Excel;
use Alma2::Analytics::DataWriter::SQL;
use Alma2::Analytics::DataWriter::SQL::SQLite;

# ========================================================================================================================

# get command line options (from input file)
my $options = get_opts();

# create the appropriate DataWriter from command line options
my $writer = make_writer($options);

if ($options->{mode} =~ /run/i) {
	# run the query (analytics results are saved in /tmp. file name indicated by --output_source)
	$writer->run();
} else {
	# save the query
	$writer->save();
}

exit;

# ========================================================================================================================

sub get_long_opts {
	my ($opts, $optf) = @_;

	# zero out options first
	$opts->{conf_file}          = undef;
	$opts->{conf_env}           = undef;
	$opts->{query_path}         = undef;
	$opts->{query_columns}      = undef;
	$opts->{query_filter_file}  = undef;
	$opts->{query_types_file}   = undef;
	$opts->{query_data_file}    = undef;
	$opts->{query_result_types} = undef;
	$opts->{query_output}       = undef;
	$opts->{output_format}      = undef;
	$opts->{output_file}        = undef;
	$opts->{output_table}       = undef;
	$opts->{output_dbasestrg}   = undef;
	$opts->{output_user}        = undef;
	$opts->{output_passwd}      = undef;

	open my $ofh, "<", $optf;
	while (<$ofh>) {
		chomp;
		my $s = $_;
		my ($opt, $val);
		if ($s =~ /^--(.*)=(.*)$/) {
			$opt = $1;
			$val = $2;
			$opts->{$opt} = $val;
		} else {
			next;
		}
	}
	close $ofh;

	return 1;
}

sub parse_opts {
	my ($options) = @_;

	# CONFIGURATION SECTION
	$options->{conf_file} = Alma2::Config::YML_FILE if ( !defined $options->{conf_file} );
	$options->{conf_env} = 'production' if ( !defined $options->{conf_env} );


	# QUERY SECTION
	# Analytics query path
	die "runrep.pl query path not specified" if ( !defined $options->{query_path} );

	# query column names
	die "runrep.pl query columns not specified" if ( !defined $options->{query_columns} );
	my $s = $options->{query_columns};
	$s =~ s/\s/_/g;
	$s =~ tr/a-z/A-Z/;
	my @cols = split('\|', $s);
	$options->{query_columns} = \@cols;

	# query filter, types and data
	if ( defined $options->{query_filter_file} ) {
		die "runrep.pl query filter file not found / not a file" if ( ! -e $options->{query_filter_file} || ! -f $options->{query_filter_file} );
		die "runrep.pl query filter data types file not found / not a file" if ( !defined $options->{query_types_file} || ! -e $options->{query_types_file} || ! -f $options->{query_types_file} );
		die "runrep.pl query filter data value file not found / not a file" if ( !defined $options->{query_data_file} || ! -e $options->{query_data_file} || ! -f $options->{query_data_file} );
		die "runrep.pl query output file not specified" if ( !defined $options->{query_output} );

		# load the filters
		my $filter_file = $options->{query_filter_file};
		$options->{query_filters} = read_filters($filter_file);
		delete $options->{query_filter_file};

		# load the corresponding filter data types
		my $types_file = $options->{query_types_file};
		$options->{query_types} = read_types($types_file);
		delete $options->{query_types_file};

		# load the corresponding filter data
		# data is expected in csv file, no header line
		# we assume each line of data must match in line for line value/column counts
		# when there is time will change the working of filter and data
		my $data_file = $options->{query_data_file};
		$options->{query_data} = read_data($data_file);
		delete $options->{query_data_file};

		# final sanity checks
		die "DataWriter.new() types and filter files mismatch. Number of rows do not match (empty line at eof maybe?)."
			if scalar @{$options->{query_filters}} != scalar @{$options->{query_types}};
		die "DataWriter.new() data and filter files mismatch. Number of rows do not match (empty line at eof maybe?)."
			if scalar @{$options->{query_filters}} != scalar @{$options->{query_data}};

	} else {
		delete $options->{query_filter_file};
		delete $options->{query_types_file};
		delete $options->{query_data_file};

		$options->{query_filters} = undef;
		$options->{query_types} = undef;
		$options->{query_data} = undef;
	}


	# OUTPUT SECTION
	die "runrep.pl output not specified" if ( ! defined $options->{output_format} );
	die "runrep.pl output file not specified" if ( !defined $options->{output_file} );

	if ( defined $options->{output_table} ) {
		# output table
		$options->{output_table} =~ s/\s/_/g;
		$options->{output_table} =~ tr/a-z/A-Z/;
	}

	die "runrep.pl output column types not specified" if ( !defined $options->{output_types} );
	my @types = split(/\|/, $options->{output_types});
	die "runrep.pl output column types list is empty" if ( ! scalar @types );
	$options->{output_types} = \@types;

	$options->{output_format} =~ tr/A-Z/a-z/r;
	die "runrep.pl output format " . $options->{output_format} . " not recognised"
		if $options->{output_format} !~ /(csv|xls|xlsx|sqlite)/i;

	if ( $options->{output_format} =~ /(mysql|oracle)/i ) {
		die "runrep.pl database connection string missing" if ( !defined $options->{output_dbasestrg} );

		if ( $options->{otuput_format} =~ /mysql/i ) {
			# expected database=$dbase,host=$host,port=$port
			my @parts = split(',', $options->{output_dbasestrg});
			die "runrep.pl database connection string for mysql is invalid. must be of format database=database,host=host,port=port,user=user,password=password" if scalar @parts != 5;

			foreach my $part (@parts) {
				my ($p, $s) = split('=', $part);
				if ($p eq 'database') {
					$options->{output_database} = $s;
				} elsif ($p eq 'host') {
					$options->{output_host} = $s;
				} elsif ($p eq 'port') {
					$options->{output_port} = $s;
				} elsif ($p eq 'user') {
					$options->{output_user} = $s;
				} elsif ($p eq 'password') {
					$options->{output_passwd} = $s;
				} else {
					die "runrep.pl database connection string for mysql contains unrecognised parameter. must be of format database=database,host=host,port=port,user=user,password=password";
				}
			}

			delete $options->{output_dbasestrg};
		} else {
			# expected database=$dbase,host=$host,port=$port
			my @parts = split('=', $options->{output_dbasestrg});
			die "runrep.pl database connection string for oracle is invalid. must be of format sid=sid,user=user,password=password" if scalar @parts != 3;

			foreach my $part (@parts) {
				my ($p, $s) = split('=', $part);
				if ($p eq 'sid') {
					$options->{output_sid} = $s;
				} elsif ($p eq 'user') {
					$options->{output_user} = $s;
				} elsif ($p eq 'password') {
					$options->{output_passwd} = $s;
				} else {
					die "runrep.pl database connection string for oracle contains unrecognised parameter. must be of format sid=sid,port=port,user=user,password=password";
				}
			}
		}
	}

	return;
}

sub read_filters {
	my ($infile) = @_;
	my @lines = slurp($infile);
	@lines = map { chomp $_; $_ } @lines;
	return \@lines;
}

sub read_types {
	my ($infile) = @_;
	my @tlines = slurp($infile);
	my @types = map { my @a = split(/\|/, $_); \@a } map { chomp $_; $_ } @tlines;
	return \@types;
}

sub read_data {
	my ($infile) = @_;

	my ($file, $path, $suffix) = fileparse($infile, qr/\.[^.]*/);
	my $dbh = DBI->connect('DBI:CSV:', undef, undef, {
		f_dir => $path,
		csv_tables => {
			data => {
				csv_skip_first_row => 0,
				f_file => "$file$suffix",
			}
		},
	});

	my @lines;
	my $qs = 'select * from data';
	my $sth = $dbh->prepare($qs) or die $DBI::errstr;
	$sth->execute;

	while ( my $row = $sth->fetchrow_arrayref ) {
		my @columns = @$row;
		push @lines, \@columns
	}

	$sth->finish;
	$dbh->disconnect;

	return \@lines;
}

sub get_opts {

	my $mode = $ARGV[0];	# either run or convert
	die "Usage: runrep.pl [run|save] <options-file>" if ( $mode !~ /(run|save)/i );
	my $options = { mode => $mode, };

	my $optfile = $ARGV[1];
	die "Usage: runrep.pl [run|save] <options-file>" if !$optfile;

	# get long options
	if ( !get_long_opts($options, $optfile) ) {
		return undef;
	}

	# parse and sanitise options
	parse_opts($options);

	return $options;
}

# ========================================================================================================================

sub make_writer {
	my ($options) = @_;

	my $fmt = $options->{output_format};
	my $writer;
	if ( $fmt =~ /csv/i ) {
		$writer = Alma2::Analytics::DataWriter::CSV->new($options);
	} elsif ( $fmt =~ /xlsx/i ) {
		$writer = Alma2::Analytics::DataWriter::Excel->new($options);
	} elsif ( $fmt =~ /xls/i ) {
		$writer = Alma2::Analytics::DataWriter::Excel->new($options);
	} elsif ( $fmt =~ /sqlite/i ) {
		$writer = Alma2::Analytics::DataWriter::SQL::SQLite->new($options);
	} else {
		die "runrep.pl $fmt is not a recognised output format.\n";
	}

	return $writer;
}

# ========================================================================================================================
