#!/data/alma/perl5/perlbrew/perls/perl-5.20.1/bin/perl

# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use Data::Dumper;

use Time::HiRes qw(gettimeofday);

use Alma2::Util::DateTime;

my $dts;
my $t2;
my $secs;

$dts = Alma2::Util::DateTime->new();
print "Test 1: " . $dts->datetimestamp('%Y-%m-%d %H:%M:%S') . "\n";

$dts = Alma2::Util::DateTime->new(time);
print "Test 2: " . $dts->datetimestamp('%Y-%m-%d %H:%M:%S') . "\n";

$dts = Alma2::Util::DateTime->new([gettimeofday()]);
print "Test 3: " . $dts->datetimestamp('%Y-%m-%d %H:%M:%S') . "\n";

$dts->set(time => time());
print "Test 4: " . $dts->datetimestamp('%Y-%m-%d %H:%M:%S') . "\n";

$dts->set(hires => [ time(), 0 ]);
print "Test 5: " . $dts->datetimestamp('%Y-%m-%d %H:%M:%S') . "\n";

$dts->set();
print "Test 6: " . $dts->datetimestamp('%Y-%m-%d %H:%M:%S') . "\n";

$dts->set(hires => [gettimeofday]);
print "Test 7: " . $dts->msecstamp('%Y-%m-%d %H:%M:%S') . "\n";

$dts->add('14 days');
print "Test 8a: " . $dts->datetimestamp('%Y-%m-%d %H:%M:%S').".".$dts->msecstamp() . "\n";
print "Test 8b: " . $dts->datetimestamp('%Y-%m-%d %H:%M:%S').".".$dts->usecstamp() . "\n";

$dts->sub('14 days');
print "Test 9a: " . $dts->datetimestamp('%Y-%m-%d %H:%M:%S').".".$dts->msecstamp() . "\n";
print "Test 9b: " . $dts->datetimestamp('%Y-%m-%d %H:%M:%S').".".$dts->usecstamp() . "\n";

$dts->add('14 days');
print "Test 10a: " . $dts->datetimestamp('%Y-%m-%d %H:%M:%S').".".$dts->msecstamp() . "\n";
print "Test 10b: " . $dts->datetimestamp('%Y-%m-%d %H:%M:%S').".".$dts->usecstamp() . "\n";

$dts->sub('17 days');
print "Test 11a: " . $dts->datetimestamp('%Y-%m-%d %H:%M:%S').".".$dts->msecstamp() . "\n";
print "Test 11b: " . $dts->datetimestamp('%Y-%m-%d %H:%M:%S').".".$dts->usecstamp() . "\n";
