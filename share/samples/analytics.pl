#!/data/alma/perl5/perlbrew/perls/perl-5.20.1/bin/perl

# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;

use Alma2::Config;
use Alma2::Config::YAML;
use Alma2::Analytics::Client;
use Alma2::Analytics::Filter;
use Alma2::Util::DateTime;
use Alma2::Util::Core qw(:all);

# ========================================================================================================================

# create analytics client
my $conf = Alma2::Config::YAML->new(Alma2::Config::YML_FILE)->get('production.analytics');
my $dbh = Alma2::Analytics::Client->new($conf);

# get isbns whose bib records were modified within a date range
get_isbns($dbh);

# get isbns / titles with their loan dates within a date range
get_loans($dbh);

# ========================================================================================================================

exit;

# ========================================================================================================================

# ====================
# get ISBNs of bib records for physical items that were modified in a given period
# ====================

sub get_isbns {

	my ($dbh) = @_;

	print "MMS_ID,ISBN,TITLE,DATE_MODIFIED,\n";

	# [0] "Physical Items"."Bibliographic Details"."ISBN" s_1,
	# [1] "Physical Items"."Bibliographic Details"."MMS Id" s_2,
	# [2] "Physical Items"."Bibliographic Details"."Modification Date" s_3,
	# [3] "Physical Items"."Bibliographic Details"."Title" s_4,
	# [4] CURRENT_DATE
	my @columns = qw( ISBN MMS_ID MODIFICATION_DATE TITLE CURRENT_DATE );
	my $qs = '/shared/Community/Reports/Curtin University/Analytics Framework/MyReport1';
	my $sth = $dbh->prepare($qs, \@columns);

	# prepare date
	my $start = Alma2::Util::DateTime->new();
	$start->set(string => '2016/01/01 00:00:00');
	my $t0 = $start->datestamp('%Y-%m-%dT%H:%M:%S');

	my $end = Alma2::Util::DateTime->new();
	$end->set(string => '2016/01/03 00:00:00');
	my $t1 = $end->datestamp('%Y-%m-%dT%H:%M:%S');

	# prepare XML filter ahead of executing query
	my $sf = 'between("Physical Items"."Bibliographic Details"."Modification Date",?,?)';
	my $types = [ 'date', 'date' ];

	my $of = Alma2::Analytics::Filter->new();
	$of->initialise($sf, $types);

	my $filter = $of->compile([ $t0, $t1]);

	$sth->bind_param($filter);

	# execute query
	$sth->execute() or die $dbh->errstr();

	# process results
	while ( my $row = $sth->fetchrow_hashref() ) {
		my $mms_id = $row->{MMS_ID};
		my $isbn = $row->{ISBN};
		my $title = $row->{TITLE};
		my $date_mod = $row->{MODIFICATION_DATE};

		print "\"$mms_id\",\"$isbn\",\"$title\",\"$date_mod\",\n";
	}

	$sth->finish();

}

# ====================
# get ISBNs of bib records for physical items that were loaned in a given period
# ====================

sub get_loans {

	my ($dbh) = @_;

	# we need to split our queries in (arbitrary) sections of 20 as Analytics can only handle a maximum filter length
	my @mms_ids = qw(
		9926371170001951	9926372010001951	9926373750001951	9926374240001951	9926377760001951
		9926385200001951	9926388720001951	9926398970001951	9926396690001951	9926401230001951
		9926401240001951	9926403500001951	9926406930001951	9926408370001951	9926409720001951
		9926409850001951	9926437170001951	9926446780001951	9926460740001951	9926461510001951
		9926461840001951	9926464310001951	9926477890001951	9926499980001951	9926505470001951
		9926507270001951	9926508370001951	9926510420001951	9926521910001951	9926523990001951
		9926532310001951	9926537670001951	9926541260001951	9926543050001951	9926543530001951
		9926545350001951	9926570410001951	9926608650001951	9926614130001951	9926614940001951
		9926617040001951	9926620570001951	9926621380001951	9926625970001951	9926628530001951
		9926633050001951	9926633130001951	9926635460001951	9926639090001951	9926639800001951
		9926642350001951	9926648690001951	9926648890001951	9926649180001951	9926672800001951
		9926673910001951	9926702320001951	9926715770001951	9926723570001951	9926731360001951
		9926748790001951	9926756110001951	9926756580001951	9926759210001951	9926763640001951
		9926769190001951	9926786240001951	9926788760001951	9926789220001951	9926793160001951
		9926793270001951	9926801850001951	9926820060001951	9926825320001951	9926826820001951
		9926832510001951	9926834340001951	9926852760001951	9926879820001951	9926895160001951
		9926916890001951	9926931190001951	9926931380001951	9926932160001951	9926945770001951
		9926950530001951	9926955290001951	9926958250001951	9926959180001951	9926959490001951
		9926969200001951	9926981680001951	9926986840001951	9926995150001951	9927005310001951
		9927005860001951	9927009000001951
	);
	my @sets = split_array(20, \@mms_ids);

	# set the dates
	my $start = Alma2::Util::DateTime->new();
	$start->set(string => '2015/12/01 00:00:00');
	my $t0 = $start->datestamp('%Y-%m-%d');

	my $end = Alma2::Util::DateTime->new();
	$end->set(string => '2015/12/31 00:00:00');
	my $t1 = $end->datestamp('%Y-%m-%d');

	# prepare filters for each set of mms ids
	my @filters;

	my $c = scalar @mms_ids;
	my $n = int($c / 20);
	my $r = $c % 20;

	my @p = map { '?' } 1..$r;
	my $s = join(',', @p);
	my $sf2 = 'and(between("Fulfillment"."Loan Date"."Loan Date",?,?),in("Physical Items"."Bibliographic Details"."MMS Id",['.$s.']))';
	my @t = map { 'string' } 1..$r;
	my @types = ( 'dateTime', 'dateTime' );
	push @types, @t;

	my $of2 = Alma2::Analytics::Filter->new();
	$of2->initialise($sf2, \@types);
	push @filters, $of2;

	@p = map { '?' } 1..20;
	$s = join(',', @p);
	my $sf1 = 'and(between("Fulfillment"."Loan Date"."Loan Date",?,?),in("Physical Items"."Bibliographic Details"."MMS Id",['.$s.']))';
	@t = map { 'string' } 1..20;
	@types = ( 'dateTime', 'dateTime' );
	push @types, @t;

	my $of1 = Alma2::Analytics::Filter->new();
	$of1->initialise($sf1, \@types);
	push @filters, $of1 for (1..$n);

	# [0]   "Fulfillment"."Loan Date"."Loan Date" s_1,
	# [1]   "Physical Items"."Bibliographic Details"."ISBN" s_2,
	# [2]   "Physical Items"."Bibliographic Details"."MMS Id" s_3,
	# [3]   "Physical Items"."Bibliographic Details"."Title" s_4
	my @columns = ( "LOAN_DATE", "ISBN", "MMS_ID", "TITLE" );
	my $qs = '/shared/Community/Reports/Curtin University/Analytics Framework/MyReport2';

	print "MMS_ID,TITLE,ISBN,LOAN_DATE,\n";

	foreach my $set (@sets) {
		my $of = pop @filters;
		my @params = ( $t0, $t1 );
		push @params, @$set;
		my $xml = $of->compile(\@params);

		my $sth = $dbh->prepare($qs, \@columns);
		$sth->bind_param($xml);
		$sth->execute() or die $dbh->errstr();

		while ( my $row = $sth->fetchrow_hashref() ) {
			my $isbn = $row->{ISBN};
			my $mms_id = $row->{MMS_ID};
			my $date = $row->{LOAN_DATE};
			my $title = $row->{TITLE};
			print "\"$mms_id\",\"$title\",\"$isbn\",\"$date\",\n";
		}

		$sth->finish();
	}

}

# ========================================================================================================================
