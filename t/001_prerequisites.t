#!/usr/bin/env perl

# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

use Test::More tests => 35;
use strict;
use warnings;

use_ok('Clone');
use_ok('DBD::CSV');
use_ok('DBD::SQLite');
use_ok('DBI');
use_ok('Data::Dumper');
use_ok('Date::Calc');
use_ok('Date::Manip');
use_ok('DateTime');
use_ok('Encode');
use_ok('Excel::Writer::XLSX');
use_ok('Exporter');
use_ok('Fcntl');
use_ok('File::Basename');
use_ok('File::Copy');
use_ok('File::Copy::Recursive');
use_ok('File::Path');
use_ok('File::Slurp');
use_ok('HOP::Lexer');
use_ok('IO::File');
use_ok('IO::Handle');
use_ok('JSON');
use_ok('LWP::UserAgent');
use_ok('MIME::Entity');
use_ok('Net::FTP');
use_ok('POSIX');
use_ok('REST::Client');
use_ok('Spreadsheet::ParseExcel');
use_ok('Spreadsheet::WriteExcel');
use_ok('Switch');
use_ok('Time::HiRes');
use_ok('URI');
use_ok('XML::LibXML');
use_ok('XML::LibXML::Reader');
use_ok('XML::LibXML::XPathContext');
use_ok('YAML');

1;
