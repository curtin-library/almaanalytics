#!/usr/bin/env perl

# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.

use Test::More tests => 4;

use strict;
use warnings;
use Data::Dumper;

use Alma2::Analytics::Filter;

# ========================================================================================================================

{
	my $str = 'and ( !null ( "Physical Items"."Bibliographic Details"."ISBN" ) , eq ( "Bibliographic Details"."Suppressed From Discovery" , ? ) , between ( "Bibliographic Details"."Modification Date" , ? , ? ) , null ( "Portfolio"."Pps link id" ) , in ( "Bibliographic Details"."MMS ID", [ ?, ?, ? ] ) )';
	my $types = [ 'string', 'date', 'date', 'integer', 'integer', 'integer' ];
	my $filter = Alma2::Analytics::Filter->new();
	$filter->initialise($str, $types);

	my $values = [ qw( No 2015-01-01 2015-12-31 1951 9511 9511 ) ];
	my $xml = $filter->compile($values);

	my $expected = '<sawx:expr xsi:type="sawx:logical" op="and" xmlns:saw="com.siebel.analytics.web/report/v1.1" xmlns:sawx="com.siebel.analytics.web/expression/v1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
<sawx:expr xsi:type="sawx:comparison" op="notNull">
<sawx:expr xsi:type="sawx:sqlExpression">"Physical Items"."Bibliographic Details"."ISBN"</sawx:expr>
</sawx:expr>
<sawx:expr xsi:type="sawx:comparison" op="equal">
<sawx:expr xsi:type="sawx:sqlExpression">"Bibliographic Details"."Suppressed From Discovery"</sawx:expr>
<sawx:expr xsi:type="xsd:string">No</sawx:expr>
</sawx:expr>
<sawx:expr xsi:type="sawx:comparison" op="between">
<sawx:expr xsi:type="sawx:sqlExpression">"Bibliographic Details"."Modification Date"</sawx:expr>
<sawx:expr xsi:type="xsd:date">2015-01-01</sawx:expr>
<sawx:expr xsi:type="xsd:date">2015-12-31</sawx:expr>
</sawx:expr>
<sawx:expr xsi:type="sawx:comparison" op="null">
<sawx:expr xsi:type="sawx:sqlExpression">"Portfolio"."Pps link id"</sawx:expr>
</sawx:expr>
<sawx:expr xsi:type="sawx:list" op="in">
<sawx:expr xsi:type="sawx:sqlExpression">"Bibliographic Details"."MMS ID"</sawx:expr>
<sawx:expr xsi:type="xsd:integer">1951</sawx:expr>
<sawx:expr xsi:type="xsd:integer">9511</sawx:expr>
<sawx:expr xsi:type="xsd:integer">9511</sawx:expr>
</sawx:expr>
</sawx:expr>
';

	is($xml, $expected, "Test 1 - Mixed types filter test");
}

{
	my $str = 'and(or(lt("Physical Items"."Bibliographic Details"."Modification Date",?),gt("Physical Items"."Bibliographic Details"."Modification Date",?)),between("Bibliographic Details"."Modification Date",?,?))';
	my $types = [ 'date', 'date', 'date', 'date' ];
	my $filter = Alma2::Analytics::Filter->new();
	$filter->initialise($str, $types);

	my $values = [ qw( 2015-01-01 2015-12-31 2014-01-01 2014-12-31 ) ];
	my $xml = $filter->compile($values);

	my $expected = '<sawx:expr xsi:type="sawx:logical" op="and" xmlns:saw="com.siebel.analytics.web/report/v1.1" xmlns:sawx="com.siebel.analytics.web/expression/v1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
<sawx:expr xsi:type="sawx:logical" op="or">
<sawx:expr xsi:type="sawx:comparison" op="less">
<sawx:expr xsi:type="sawx:sqlExpression">"Physical Items"."Bibliographic Details"."Modification Date"</sawx:expr>
<sawx:expr xsi:type="xsd:date">2015-01-01</sawx:expr>
</sawx:expr>
<sawx:expr xsi:type="sawx:comparison" op="greater">
<sawx:expr xsi:type="sawx:sqlExpression">"Physical Items"."Bibliographic Details"."Modification Date"</sawx:expr>
<sawx:expr xsi:type="xsd:date">2015-12-31</sawx:expr>
</sawx:expr>
</sawx:expr>
<sawx:expr xsi:type="sawx:comparison" op="between">
<sawx:expr xsi:type="sawx:sqlExpression">"Bibliographic Details"."Modification Date"</sawx:expr>
<sawx:expr xsi:type="xsd:date">2014-01-01</sawx:expr>
<sawx:expr xsi:type="xsd:date">2014-12-31</sawx:expr>
</sawx:expr>
</sawx:expr>
';

	is($xml, $expected, "Test 2 - Date type filter test");
}


{
	my $str = qq/and(
 or(
  ge("Physical Items"."Bibliographic Details"."Modification Date",?),
  le("Physical Items"."Bibliographic Details"."Modification Date",?)
 ),
 between("Bibliographic Details"."Modification Date",?,?),
 !null("Bibliographic Details"."ISBN")
)/;
	my $types = [ 'date', 'date', 'date', 'date' ];
	my $filter = Alma2::Analytics::Filter->new();
	$filter->initialise($str, $types);

	my $values = [ qw( 2015-01-01 2015-12-31 2014-01-01 2014-12-31 ) ];
	my $xml = $filter->compile($values);
	
	my $expected = '<sawx:expr xsi:type="sawx:logical" op="and" xmlns:saw="com.siebel.analytics.web/report/v1.1" xmlns:sawx="com.siebel.analytics.web/expression/v1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
<sawx:expr xsi:type="sawx:logical" op="or">
<sawx:expr xsi:type="sawx:comparison" op="greaterOrEqual">
<sawx:expr xsi:type="sawx:sqlExpression">"Physical Items"."Bibliographic Details"."Modification Date"</sawx:expr>
<sawx:expr xsi:type="xsd:date">2015-01-01</sawx:expr>
</sawx:expr>
<sawx:expr xsi:type="sawx:comparison" op="lessOrEqual">
<sawx:expr xsi:type="sawx:sqlExpression">"Physical Items"."Bibliographic Details"."Modification Date"</sawx:expr>
<sawx:expr xsi:type="xsd:date">2015-12-31</sawx:expr>
</sawx:expr>
</sawx:expr>
<sawx:expr xsi:type="sawx:comparison" op="between">
<sawx:expr xsi:type="sawx:sqlExpression">"Bibliographic Details"."Modification Date"</sawx:expr>
<sawx:expr xsi:type="xsd:date">2014-01-01</sawx:expr>
<sawx:expr xsi:type="xsd:date">2014-12-31</sawx:expr>
</sawx:expr>
<sawx:expr xsi:type="sawx:comparison" op="notNull">
<sawx:expr xsi:type="sawx:sqlExpression">"Bibliographic Details"."ISBN"</sawx:expr>
</sawx:expr>
</sawx:expr>
';

	is($xml, $expected, "Test 3 - Multi-line filter test");
}

{
	my $str = qq/and (
	between ( "Bibliographic Details"."Modification Date", ?, ? ),
	in ( "Bibliographic Details"."MMS Id", [ ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ] ),
	or (
		!null ( "Bibliographic Details"."ISBN" ),
		!null ( "Bibliographic Details"."ISSN" )
	),
	like (
		"Physical Item Details"."Barcode",
		[ ? ]
	),
	!like (
		"Physical Item Details"."Barcode",
		[ ? ]
	),
	le ( "Bibliographic Details"."MMS Id", ? )
)/;
	my $types = [ 'date', 'date', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string' ];
	my $filter = Alma2::Analytics::Filter->new();
	$filter->initialise($str, $types);

	my $values = [ qw(
		2015-01-01Z 2015-12-31Z
		9926371020001951 9926371030001951 9926371040001951 9926371050001951 9926371060001951 9926371070001951 9926371080001951 9926371090001951 9926371100001951 9926371110001951
		DC%
		dc%
		9926373560001951
	)];
	my $xml = $filter->compile($values);

	my $expected = '<sawx:expr xsi:type="sawx:logical" op="and" xmlns:saw="com.siebel.analytics.web/report/v1.1" xmlns:sawx="com.siebel.analytics.web/expression/v1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
<sawx:expr xsi:type="sawx:comparison" op="between">
<sawx:expr xsi:type="sawx:sqlExpression">"Bibliographic Details"."Modification Date"</sawx:expr>
<sawx:expr xsi:type="xsd:date">2015-01-01Z</sawx:expr>
<sawx:expr xsi:type="xsd:date">2015-12-31Z</sawx:expr>
</sawx:expr>
<sawx:expr xsi:type="sawx:list" op="in">
<sawx:expr xsi:type="sawx:sqlExpression">"Bibliographic Details"."MMS Id"</sawx:expr>
<sawx:expr xsi:type="xsd:string">9926371020001951</sawx:expr>
<sawx:expr xsi:type="xsd:string">9926371030001951</sawx:expr>
<sawx:expr xsi:type="xsd:string">9926371040001951</sawx:expr>
<sawx:expr xsi:type="xsd:string">9926371050001951</sawx:expr>
<sawx:expr xsi:type="xsd:string">9926371060001951</sawx:expr>
<sawx:expr xsi:type="xsd:string">9926371070001951</sawx:expr>
<sawx:expr xsi:type="xsd:string">9926371080001951</sawx:expr>
<sawx:expr xsi:type="xsd:string">9926371090001951</sawx:expr>
<sawx:expr xsi:type="xsd:string">9926371100001951</sawx:expr>
<sawx:expr xsi:type="xsd:string">9926371110001951</sawx:expr>
</sawx:expr>
<sawx:expr xsi:type="sawx:logical" op="or">
<sawx:expr xsi:type="sawx:comparison" op="notNull">
<sawx:expr xsi:type="sawx:sqlExpression">"Bibliographic Details"."ISBN"</sawx:expr>
</sawx:expr>
<sawx:expr xsi:type="sawx:comparison" op="notNull">
<sawx:expr xsi:type="sawx:sqlExpression">"Bibliographic Details"."ISSN"</sawx:expr>
</sawx:expr>
</sawx:expr>
<sawx:expr xsi:type="sawx:list" op="like">
<sawx:expr xsi:type="sawx:sqlExpression">"Physical Item Details"."Barcode"</sawx:expr>
<sawx:expr xsi:type="xsd:string">DC%</sawx:expr>
</sawx:expr>
<sawx:expr xsi:type="sawx:list" op="notLike">
<sawx:expr xsi:type="sawx:sqlExpression">"Physical Item Details"."Barcode"</sawx:expr>
<sawx:expr xsi:type="xsd:string">dc%</sawx:expr>
</sawx:expr>
<sawx:expr xsi:type="sawx:comparison" op="lessOrEqual">
<sawx:expr xsi:type="sawx:sqlExpression">"Bibliographic Details"."MMS Id"</sawx:expr>
<sawx:expr xsi:type="xsd:string">9926373560001951</sawx:expr>
</sawx:expr>
</sawx:expr>
';

	is($xml, $expected, "Test 4 - Multiple block values test");
}

1;
